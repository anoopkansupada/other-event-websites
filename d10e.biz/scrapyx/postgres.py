# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from __future__ import print_function
import httplib2
import os

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from pprint import pprint
import logging
from scrapy.exceptions import DropItem
from scrapy.http import Request
import sys
import psycopg2
from scrapy.utils.project import get_project_settings
import json
  
class Postgres(object): 
  settings = []
  
  def __init__(self, *args, **kwargs):
    super(Postgres, self).__init__(*args, **kwargs)
    logging.info("Init Postgres")
    
    project_settings = get_project_settings()

    with open('config.json', 'r') as ip:
      _config = json.loads(ip.read())            
      config = _config[_config['environment']]    
      db_config = config['db']
      self.settings = db_config
      
  def connect(self):    
    # connect to postgres
    connection = psycopg2.connect(
                database=self.settings.get('DB_NAME'),
                user=self.settings.get('DB_USER'),
                host=self.settings.get('DB_HOST'),
                password=self.settings.get('DB_PASS'),
                port=self.settings.get('DB_PORT')
              )
    
    return connection
    
  def insert(self, connection, table_name, data):
    cursor = connection.cursor()
    aFormat = []        

    sColumns = ', '.join(data.keys())

    for key in data.keys():
      aFormat.append("%s")            
    sFormat = ', '.join(aFormat)    

    result = False
    
    try:      
      cursor.execute("""INSERT INTO """ + table_name + """ (""" + sColumns + """) 
      VALUES(""" + sFormat + """)""", 
      data.values())      
      cursor.execute('SELECT LASTVAL()')
      result = cursor.fetchone()[0]
      #print result  
   
    except psycopg2.DatabaseError, e:
      logging.info("DatabaseError: %s", e)
      connection.rollback()     
    except psycopg2.InterfaceError as exc:  
      logging.info("InterfaceError: %s", exc)
      connection = psycopg2.connect(
                database=self.settings.get('DB_NAME'),
                user=self.settings.get('DB_USER'),
                host=self.settings.get('DB_HOST'),
                password=self.settings.get('DB_PASS'),
                port=self.settings.get('DB_PORT')
              )
      cursor = connection.cursor()
    finally:
      connection.commit()  

    return result

  def insert_multiple(self, connection, table_name, data):
    cursor = connection.cursor()
    aFormat = []        

    sColumns = ', '.join(data[0].keys())

    for key in data[0].keys():
      aFormat.append("%s")            
    sFormat = ', '.join(aFormat)    
    
    _return = False
    try:              
      args_str = ','.join(cursor.mogrify("(" + sFormat + ")", x.values()) for x in data)
      result = cursor.execute("INSERT INTO " + table_name + "(" + sColumns + ") VALUES " + args_str)       
   
    except psycopg2.DatabaseError, e:
      logging.info("DatabaseError: %s", e)
      connection.rollback()     
    except psycopg2.InterfaceError as exc:  
      logging.info("InterfaceError: %s", exc)
      connection = psycopg2.connect(
                database=self.settings.get('DB_NAME'),
                user=self.settings.get('DB_USER'),
                host=self.settings.get('DB_HOST'),
                password=self.settings.get('DB_PASS'),
                port=self.settings.get('DB_PORT')
              )
      cursor = connection.cursor()
    finally:
      connection.commit()
      _return = True

    return _return
    
  def select(self, connection, query):
    cursor = connection.cursor()    
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows
    
  def update(self, connection, query):
    cursor = connection.cursor()    
    try:
      result = cursor.execute(query)
      return result
    except psycopg2.DatabaseError, e:
      logging.info("DatabaseError: %s", e)
      connection.rollback()     
    finally:
      connection.commit()
    
  def execute(self, connection, query):
    cursor = connection.cursor()    
    try:
      result = cursor.execute(query)
      return result
    except psycopg2.DatabaseError, e:
      logging.info("DatabaseError: %s", e)
      connection.rollback()
    finally:
      connection.commit()
