# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class D10eSpider(scrapy.Spider):
    name = "d10e"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(D10eSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        self.all_companies = self._air_company_table.get_all()

        self.speaker_panel = {}

    def parse(self, response):      
      self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
      confs = self._air_confer_table.search('Backend', 'd10e')
      deny_url = ["http://d10e.biz/", "http://d10e.biz", "http://www.d10e.biz", "http://www.d10e.biz/"]
      
      for x in confs:        
        meta = {
          'conf': x
        }
        if 'Sponsor URL' not in x['fields'].keys():
          continue
        _url = x['fields']['Sponsor URL']   
        """
        if _url == 'http://d10e.biz/tel-aviv-2018/':
          req = scrapy.Request(_url, callback=self.parse_details, meta = meta)
          yield req

        """
        
        if _url in deny_url:
          continue
        req = scrapy.Request(_url, callback=self.parse_details, meta = meta)
        yield req

        #"""

    def parse_details(self, response):
      meta = response.meta

      # Parse panels
      
      if response.xpath('.//div[contains(@class, "scheduled-events") and contains(@class, "desktop")]'):
        for div in response.xpath('.//div[contains(@class, "scheduled-events") and contains(@class, "desktop")]//div[contains(@class, "scheduled-event")]'):
          date = div.xpath('.//@data-date').extract_first()          

          time_start = div.xpath('.//span[@class="time-starts"]//text()').extract_first()
          time_end = ''
          if div.xpath('.//span[@class="time-ends"]'):
            time_end = div.xpath('.//span[@class="time-ends"]//text()').extract_first()

          start_time_string = date + ' ' + time_start

          if time_end != '':
            end_time_string = date + ' ' + time_end

          ts = time.mktime(datetime.datetime.strptime(start_time_string, "%Y-%m-%d %H:%M").timetuple())
          session_id = str(ts).split('.')[0]

          event_titles = div.xpath('.//div[contains(@class, "event-title")]//text()').extract()
          topic = event_titles[0].encode('utf-8')
          speaker_name = ''
          speaker_position = ''
          if len(event_titles) == 2:
            speaker = event_titles[1]
            speaker_parts = speaker.split(',')
            if len(speaker_parts) > 1:
              speaker_name = speaker_parts[0].strip()
              speaker_position = ','.join(speaker_parts[1:]).strip()
              #print (speaker_name, speaker_position)

          #print (date, time_start, time_end, event_titles)

          """ Insert into PANELS """

          _air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)

          panel_fields = {
            'Topic': topic,
            'Date': date,
            'Link to Conference': [meta['conf']['id']],
            'Start Time': self.convert_time(start_time_string),   
            'URL of Panel': response.url,  
            'Panel ID': session_id,                             
          }

          if time_end != '':
            panel_fields.update({
              'End Time': self.convert_time(end_time_string)
            })

          _search_term = str(topic) + str(' @ "') + str(meta['conf']['fields']['ID']) + '"'
          if session_id:
            _search_term = _search_term + ' @ ' + 'pid' + str(session_id)
                
          search_term = _search_term.replace("'", r"\'")
          panels = _air_panels_table.search('Name', search_term)

          if not panels:                
            panel = _air_panels_table.insert(panel_fields)
          else:
            panel = panels[0]
            _air_panels_table.update(panel['id'], panel_fields)  

          panel_id = panel['id']    

          if speaker_name and speaker_position:
            self.speaker_panel[speaker_name.lower()] = {
              'speaker_name': speaker_name,
              'speaker_position': speaker_position,
              'panel_id': panel_id,
              'conf_id': meta['conf']['id']
            }          

          """ End Insert into PANELS """    
      #print self.speaker_panel
      self.insert_individual(self.speaker_panel)

      # Parse sponsors
      # May be manualy scraping
      for div in response.xpath('.//div[@id="sponsors"]//div[contains(@class, "wpb_single_image")]'):
        sponsor_img = div.xpath('.//div[contains(@class, "vc_single_image-wrapper")]//img//@src').extract_first()
        sponsor_url = div.xpath('.//a[1]//@href').extract_first()
        #print (sponsor_url, sponsor_img)


      # Parse Speakers
      for article  in response.xpath('.//div[contains(@class, "projects_holder_outer")]//div[contains(@class, "portfolio_main_holder")]//article'):
        profile_url = article.xpath('.//a[1]//@href').extract_first()
        profile_img = article.xpath('.//span[@class="image"]//img//@src').extract_first()

        #print (profile_url, profile_img)
        indiv_meta = {
          'profile_img': profile_img,
          'profile_url': profile_url,
          'conf_id': meta['conf']['id']
        }
        req = scrapy.Request(profile_url, callback=self.parse_individual, meta = indiv_meta)
        yield req

    def insert_individual(self, speakers):
      for x in speakers.values():
        speaker_name = x['speaker_name']
        panel_id = x['panel_id']
        conf_id = x['conf_id']
        speaker_position = x['speaker_position']

        name_parts = speaker_name.split(' ')
        firstname = ' '.join(name_parts[:-1])
        lastname = name_parts[-1]
        """ Insert individual """
        indiv_fields = {
          'F. Name': firstname.replace(u"\u2019", "'").encode('utf-8'),
          'L. Name': lastname.replace(u"\u2019", "'").encode('utf-8'),          
          'Link to Conf': [conf_id]
        }        

        search_speaker_name = speaker_name.replace(u"\u2019", "'").encode('utf-8').replace("'", r"\'")

        indivs = self._air_indiv_table.search('Name', str(search_speaker_name))

        if not indivs:                            
          indiv = self._air_indiv_table.insert(indiv_fields)
          indiv_id = indiv['id']
        else:
          # Update person
          indiv_id = indivs[0]['id']  
          update = self._air_indiv_table.update(indiv_id, indiv_fields)

          """ Insert person to 'INDIVIDUAL ROLE AT PANEL' """          
          role_at_panel_fields = {
                'Link to Panel': [panel_id],
                'Link to Person': [indiv_id],
                'Role': ['Speaker']
              }

          if ',' in search_speaker_name:
            _person_name = '"' + search_speaker_name + '"'
          else:
            _person_name = search_speaker_name
          indiv_role_at_panel_s = self._air_role_panel_table.search('Link to Person', _person_name)
          
          iCnt = 0
          if len(indiv_role_at_panel_s):          
            for i in indiv_role_at_panel_s:
              if 'Link to Panel' in i[u'fields'].keys():
                print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
                if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
                  self._air_role_panel_table.update(i['id'], role_at_panel_fields)
                  iCnt += 1                      

          if iCnt == 0:
            # Insert to 'INDIVIDUAL ROLE AT PANEL'
            inserted = self._air_role_panel_table.insert(role_at_panel_fields)

          """ End Insert person to 'INDIVIDUAL ROLE AT PANEL' """
        """ End Insert individual """

        """ Insert Company """
        indiv_role = ''
        if speaker_position != '':
          job_title_parts = speaker_position.split(' at ')

          if len(job_title_parts) > 1:
            company = job_title_parts[-1]
            indiv_role = ' '.join(job_title_parts[:-1])

            
        
            inserted_companies = {}
            for x in self.all_companies:
              if 'Name' in x['fields'].keys():
                company_name = x['fields']['Name']

                clean_company_name = self.clean_company(company_name)
                company_id = x['id']   
                inserted_companies[clean_company_name] = company_id        

              inserted_companies_keys = inserted_companies.keys()    
            
            insert_com_fields = {
              'Name': company
            }
            clean_company_name = self.clean_company(company)

            if clean_company_name in inserted_companies_keys:
              company_id = inserted_companies[clean_company_name]
            else:
              # Insert company
              inserted_company = self._air_company_table.insert(insert_com_fields)
              company_id = inserted_company['id'] 
              self.all_companies.append(inserted_company)           

        """ End Insert Company """

        """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
        if speaker_position != '':
          role_at_company_fields = {
            'Link to Individual': [indiv_id],
            'Link to Company': [company_id],
            'Role': 'Employee'
          }

          if indiv_role != '':          
            role_at_company_fields.update({'Official Title': indiv_role})

          _person_name = str(search_speaker_name)
          if ',' in _person_name:
            _person_name = '"' + _person_name + '"'

          indiv_role_at_company_s = self._air_role_at_company_table.search('Link to Individual', _person_name)
          
          iCnt = 0
          if len(indiv_role_at_company_s):          
            for i in indiv_role_at_company_s:
              if 'Link to Company' in i['fields'].keys():
                print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
                if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                  iCnt += 1                      

          if iCnt == 0:
            # Insert to 'INDIVIDUAL ROLE AT COMPANY'
            inserted = self._air_role_at_company_table.insert(role_at_company_fields)
        """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

    def parse_individual(self, response):
      meta = response.meta

      speaker_name = ''
      linkedin_url = ''
      twitter_url = ''
      job_title = ''
      indiv_role = ''
      bio = ''
      is_manual = True

      if 'portfolio_page' in response.url:
        speaker_name = response.xpath('.//div[@class="title_subtitle_holder"]//h1//span//text()').extract_first()
        speaker_name = string.capwords(speaker_name)
      else:
        is_manual = False
        div = response.xpath('.//div[contains(@class, "default_template_holder")]//div[contains(@class, "full_section_inner")]//div[3]')
        speaker_name = div.xpath('.//div/div/div[1]/div/h2/span//text()').extract_first()
        if speaker_name:
          speaker_name = string.capwords(speaker_name)
        else:
          return

        job_title = div.xpath('.//div/div/div[2]/div/p[1]/span//text()').extract_first()
        if job_title:
          job_title = string.capwords(job_title)

        bio = div.xpath('.//div/div/div[4]/div/p//text()').extract_first()

        linkedin_url = ''
        twitter_url = ''
        socials = div.xpath('.//span[contains(@class, "q_social_icon_holder")]//a//@href').extract()
        if socials:
          for x in socials:
            if 'linkedin.com' in x:
              linkedin_url = x
            if 'twitter.com' in x:
              twitter_url = x

      name_parts = speaker_name.split(' ')
      firstname = ''
      if len(name_parts) > 1:
        firstname = ' '.join(name_parts[:-1])
      lastname = name_parts[-1]

      """ Insert individual """
      indiv_fields = {
        'F. Name': firstname.replace(u"\u2019", "'").encode('utf-8'),
        'L. Name': lastname.replace(u"\u2019", "'").encode('utf-8'),
        'Pic': [{'url': meta['profile_img']}],
        'Linkedin': linkedin_url if linkedin_url else '',
        'Twitter': twitter_url if twitter_url else '',
        'Biography': bio if bio else '',    
        'Individual Link': meta['profile_url'],
        'Link to Conf': [meta['conf_id']]
      }
      
      indiv_fields.update({
            'Is Manual': 'Yes' if is_manual else 'No'
          })

      search_speaker_name = speaker_name.replace(u"\u2019", "'").encode('utf-8').replace("'", r"\'")

      if not firstname:
        search_speaker_name = ' ' + search_speaker_name

      indivs = self._air_indiv_table.search('Name', str(search_speaker_name))

      if not indivs:                            
        indiv = self._air_indiv_table.insert(indiv_fields)
        indiv_id = indiv['id']
      else:
        # Update person
        indiv_id = indivs[0]['id']  
        update = self._air_indiv_table.update(indiv_id, indiv_fields)
      """ End Insert individual """

      """ Insert Company """
      company_id = None
      if job_title != '':
        job_title_parts = job_title.split(',')

        if len(job_title_parts) > 1:
          company = job_title_parts[-1]
          indiv_role = ' '.join(job_title_parts[:-1])

          
      
          inserted_companies = {}
          for x in self.all_companies:
            if 'Name' in x['fields'].keys():
              company_name = x['fields']['Name']

              clean_company_name = self.clean_company(company_name)
              company_id = x['id']   
              inserted_companies[clean_company_name] = company_id        

          inserted_companies_keys = inserted_companies.keys()    
          
          insert_com_fields = {
            'Name': company
          }
          clean_company_name = self.clean_company(company)

          if clean_company_name in inserted_companies_keys:
            company_id = inserted_companies[clean_company_name]
          else:
            # Insert company
            inserted_company = self._air_company_table.insert(insert_com_fields)
            company_id = inserted_company['id'] 
            self.all_companies.append(inserted_company)           

      """ End Insert Company """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
      if job_title != '' and company_id:
        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': ['Employee']
        }

        if indiv_role != '':          
          role_at_company_fields.update({'Official Title': indiv_role})

        _person_name = str(search_speaker_name)
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'

        indiv_role_at_company_s = self._air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            if 'Link to Company' in i['fields'].keys():
              print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
              if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = self._air_role_at_company_table.insert(role_at_company_fields)
      """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
