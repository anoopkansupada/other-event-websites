# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 
import string

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class WbcconfSpider(scrapy.Spider):
    name = "wbc_conf"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(WbcconfSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        #self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        #self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        #self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        #self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        #self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all conferences
        #self.all_confs = []
        self.all_confs = self._air_confer_table.get_all()

        self.iCnt = 0

    def parse(self, response):    
      scripts = response.xpath('.//script[contains(text(), "startDate")]')
      for script in scripts:
        script_text = script.xpath('.//text()').extract_first()        
        name = string.capwords(re.findall('.*"name":\s"(.*?)".*', script_text)[0])
        description = re.findall('.*"description":\s"(.*?)".*', script_text)[0]
        url = re.findall('.*"url":\s"(.*?)".*', script_text)[0]
        startDate = re.findall('.*"startDate":\s"(.*?)".*', script_text)[0][:-6]
        endDate = re.findall('.*"endDate":\s"(.*?)".*', script_text)[0][:-6]
        city = re.findall('.*"addressLocality":\s"(.*?)".*', script_text)[0]
        country = re.findall('.*"addressCountry":\s"(.*?)".*', script_text)[0]
        if not city:
          city = country
        year = startDate.split('-')[0]
      
        if startDate:
          startDate = datetime.datetime.strptime(startDate, '%Y-%m-%d').strftime('%Y-%m-%d')
        if endDate:
          endDate = datetime.datetime.strptime(endDate, '%Y-%m-%d').strftime('%Y-%m-%d')

        print (startDate, endDate, name,description, url, city, country)

        search_term = name + ' (' + ', '.join([city, year]) + ')'
        
        bExist = False
        conf = None
        for x in self.all_confs:
          ID = x['fields']['ID']
          if ID.lower() == search_term.lower():
            bExist = True
            conf = x
            break

        #search_term = search_term.replace("'", r"\'")
        #confs = self._air_confer_table.search('ID', str(search_term))

        conf_fields = {          
          'Name': name,
          'Year': year,
          'URL': url,          
          'City': city,
          'Country': country,
          'Notes': 'HB added 8/27',
          'Website': response.url,
          'Backend': ['WBC']
        }

        if startDate:
          conf_fields.update({'Start Date': startDate})
        if endDate:
          conf_fields.update({'End Date': endDate})

        if not bExist:        
          print 'Inserting.....'
          conf = self._air_confer_table.insert(conf_fields)
          self.all_confs.append(conf)
        else:          
          print 'Updating.....'
          self._air_confer_table.update(conf['id'], conf_fields)      

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
