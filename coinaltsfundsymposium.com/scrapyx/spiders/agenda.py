# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable
from scrapy.linkextractors import LinkExtractor
import hashlib

class AgendaSpider(scrapy.Spider):
    name = "agenda"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(AgendaSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        #domain = get_domain(start_url)
        #self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        self.all_companies = self._air_company_table.get_all()
        #self.all_companies = []

        self.iCnt = 0

        self.lx_sections = LinkExtractor(restrict_xpaths=(),
                                allow=r'/schedule/', deny=())

    def parse(self, response):      
      search_term = 'CoinAlts Fund Symposium (San Francisco, 2018)'
      confs = self._air_confer_table.search('ID', str(search_term))
      #print conf
      #sys.exit()
      for x in confs:                
        _url = x['fields']['URL']   
        meta = {
          'conf': x,
          'original_url': _url
        }
      
        req = scrapy.Request(_url, callback=self.parse_agenda, meta = meta)
        yield req

    def parse_agenda(self, response):
      meta = response.meta

      all_speakers = []

      days = response.css('div.full-agenda>div.content>h5::text').extract()
      schedules = response.css('div.full-agenda>div.content>table')

      for i, day in enumerate(days):
        schedule = schedules[i]
        date = ', '.join(day.split(', ')[1:])
        _date_only = datetime.datetime.strptime(date, '%B %d, %Y').strftime('%m/%d/%Y')
        for row in schedule.xpath('.//tr'):
          times = row.xpath('.//td[1]//ce//text()').extract_first()
          title = row.xpath('.//td[2]//ce//h6//text()').extract_first()

          if times:
            times = times.replace(u'\u2013', '')
            time_part = times.split(' to ')
            start_time_string = str(_date_only) + ' ' + time_part[0].strip().upper()
            start_time = self.convert_time_2(start_time_string)
            end_time_string = str(_date_only) + ' ' + time_part[1].strip().upper()
            end_time = self.convert_time_2(end_time_string)

            topic = title.encode('utf-8')
            session_id = self.md5hash(times)

            panel_fields = {
              'Topic': topic,                
              'Link to Conference': [meta['conf']['id']],  
              'Date': str(_date_only),
              'Start Time': start_time,
              'End Time': end_time,
              'Panel ID': session_id                                                                                    
            }
            #print panel_fields

            _search_term = str(topic) + str(' @ "') + str(meta['conf']['fields']['ID']) + '"'

            if session_id:
              _search_term = _search_term + ' @ ' + 'pid' + str(session_id)            

            search_term = _search_term.replace("'", r"\'")
            panels = self._air_panels_table.search('Name', search_term, fields = ['Name'])

            if not panels:    
              print 'Inserting panel.......'
              panel = self._air_panels_table.insert(panel_fields)
            else:
              print 'Updating panel.......'
              panel = panels[0]
              self._air_panels_table.update(panel['id'], panel_fields)
            
            panel_id = panel['id']
            
            list_speakers = []
            for sub_tr in row.xpath('.//td[2]//ce//table//tr'):
              _title = sub_tr.xpath('.//td[1]//text()').extract_first()
              speakers = sub_tr.xpath('.//td[2]//text()').extract()
              if 'Location' in _title:
                break
              #print (_title, speakers)
              if 'Moderator' in _title:
                for spk in speakers:
                  spk_name = spk.split(',')[0].replace(u'\xa0', '').strip()
                  spk_role = 'Moderator'
                  spk_company = spk.split(',')[-1].strip()
                  list_speakers.append([spk_name, spk_role, panel_id, spk_company])
              elif 'Panelists' in _title:
                for spk in speakers:
                  spk_name = spk.split(',')[0].replace(u'\xa0', '').strip()
                  spk_role = 'Speaker'
                  spk_company = spk.split(',')[-1].strip()
                  list_speakers.append([spk_name, spk_role, panel_id, spk_company])
              else:
                spk_name = _title.split(',')[0].replace(u'\xa0', '').strip()
                spk_role = 'Speaker'
                spk_company = _title.split(',')[-1].strip()
                list_speakers.append([spk_name, spk_role, panel_id, spk_company])
            
            for spk in list_speakers:
              print spk[0]
              _id = response.xpath('.//div[@class="name"]//ce[contains(text(), "' + spk[0] + '")]//@oet').extract_first()
              if not _id:
                company = spk[3]
                speaker_photo = ''
                speaker_role = ''
                bio = ''
              else:
                _id = int(_id.replace('sbb', ''))

                company = ''
                speaker_role = ''
                speaker_title = response.xpath('.//div[@class="name"]//ce[@oet="sbb' + str(_id + 1)+ '"]//text()').extract_first()
                if speaker_title:
                  speaker_titles = speaker_title.split(', ')
                  if len(speaker_titles) > 1:
                    company = speaker_titles[-1].strip()
                    speaker_role = ', '.join(speaker_titles[:-1]).strip()
                
                bio = ''
                spk_bio = response.xpath('.//div[@class="bio"]//ce[@oet="sbb' + str(_id + 2)+ '"]//text()').extract()
                if spk_bio:        
                  aBio = [x.strip() for x in spk_bio if x]
                  newBio = []
                  for t in aBio:
                    if t and t[-1] == '.':
                      t = t + '\n'

                    if t:
                      newBio.append(t)

                  bio = ' '.join(newBio).encode('utf-8')

                speaker_photo = 'https://www.coinaltsfundsymposium.com/biopics/' + str(_id + 4) + '.jpg'

              spk_item = {
                'spk_name': spk[0],
                'spk_role_at_panel': spk[1],
                'panel_id': spk[2],
                'company': company,
                'speaker_role': speaker_role,
                'bio': bio,
                'speaker_photo': speaker_photo,
                'conf_id': meta['conf']['id']
              }
              #print spk_item
              all_speakers.append(spk_item)

      for x in all_speakers:
        self.parse_individual(x)

    def parse_individual(self, spk):
      speaker_name = spk['spk_name']            
      company = spk['company']
      speaker_role = spk['speaker_role']      
      bio = spk['bio']
      speaker_photo = spk['speaker_photo']
      spk_role_at_panel = spk['spk_role_at_panel']
      
      name_parts = speaker_name.split(' ')
      firstname = ' '.join(name_parts[:-1])
      lastname = name_parts[-1]

      """ Insert to table 'INDIVIDUALS' """
      indiv_fields = {
        'F. Name': firstname.replace(u"\u2019", "'").encode('utf-8'),
        'L. Name': lastname.replace(u"\u2019", "'").encode('utf-8'),                                
        'Link to Conf': [spk['conf_id']],
      }

      if speaker_photo:
        indiv_fields.update({
            'Pic': [{'url': speaker_photo}],
          })
      if bio:
        indiv_fields.update({
            'Biography': bio
          })

      search_speaker_name = speaker_name.replace(u"\u2019", "'").encode('utf-8').replace("'", r"\'")

      indivs = self._air_indiv_table.search('Name', str(search_speaker_name))

      if not indivs:                            
        indiv = self._air_indiv_table.insert(indiv_fields)
        indiv_id = indiv['id']
      else:
        # Update person
        indiv_id = indivs[0]['id']  
        update = self._air_indiv_table.update(indiv_id, indiv_fields)
      """ End Insert to table 'INDIVIDUALS' """

      """ Insert person to 'INDIVIDUAL ROLE AT PANEL' """      
      panel_id = spk['panel_id']
      role_at_panel_fields = {
            'Link to Panel': [panel_id],
            'Link to Person': [indiv_id],
            'Role': [spk_role_at_panel]
          }

      if ',' in search_speaker_name:
        _person_name = '"' + search_speaker_name + '"'
      else:
        _person_name = search_speaker_name
      indiv_role_at_panel_s = self._air_role_panel_table.search('Link to Person', _person_name)
      
      iCnt = 0
      if len(indiv_role_at_panel_s):          
        for i in indiv_role_at_panel_s:
          print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
          if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
            self._air_role_panel_table.update(i['id'], role_at_panel_fields)
            iCnt += 1                      

      if iCnt == 0:
        # Insert to 'INDIVIDUAL ROLE AT PANEL'
        inserted = self._air_role_panel_table.insert(role_at_panel_fields)

      """ End Insert person to 'INDIVIDUAL ROLE AT PANEL' """

      """ Insert 'COMPANIES' """  

      if company:    
        inserted_companies = {}
        for x in self.all_companies:
          if 'Name' in x['fields'].keys():
            company_name = x['fields']['Name']

            clean_company_name = self.clean_company(company_name)
            company_id = x['id']   
            inserted_companies[clean_company_name] = company_id        

        inserted_companies_keys = inserted_companies.keys()    
        
        insert_com_fields = {
          'Name': company.strip()
        }
        
        clean_company_name = self.clean_company(company)

        if clean_company_name in inserted_companies_keys:
          company_id = inserted_companies[clean_company_name]
        else:
          # Insert company
          inserted_company = self._air_company_table.insert(insert_com_fields)
          company_id = inserted_company['id'] 
          self.all_companies.append(inserted_company)           

      """ End Insert Company """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
      if company and speaker_role:
        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': ['Employee'],
          'Official Title': speaker_role
        }

        _person_name = str(search_speaker_name)
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'
        else:
          _person_name = search_speaker_name

        indiv_role_at_company_s = self._air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            if 'Link to Company' in i['fields'].keys():
              print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
              if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = self._air_role_at_company_table.insert(role_at_company_fields)

        """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      fmt1 = '%m/%d/%Y %I:%M %p'
      fmt2 = '%m/%d/%Y %I:%M%p'
      try:
        datetime_object = datetime.datetime.strptime(time_string, fmt1)
      except:
        try:
          datetime_object = datetime.datetime.strptime(time_string, fmt2)
        except:
          print ('Error date format')

      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def md5hash(self, _string):
      _hex = hashlib.md5(_string).hexdigest()
      return _hex
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
