# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 
import string

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable
from scrapy.linkextractors import LinkExtractor

class SpeakerSpider(scrapy.Spider):
    name = "speaker"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(SpeakerSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        #domain = get_domain(start_url)
        #self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        #self.all_companies = []
        self.all_companies = self._air_company_table.get_all()

        self.iCnt = 0

        self.lx_sections = LinkExtractor(restrict_xpaths=(),
                                allow=(r'/speakers/'), deny=())

        self.lx_speakers = LinkExtractor(restrict_xpaths=(),
                                allow=(r'/speaker/'), deny=())

    def parse(self, response):      
      search_term = 'The North American Bitcoin Conference (Miami, 2018)'
      confs = self._air_confer_table.search('ID', str(search_term))
      
      for x in confs:                
        _url = x['fields']['URL']   
        meta = {
          'conf': x,
          'original_url': _url
        }
        """
        if 'riyadh.worldblockchainsummit.com' in _url:
          req = scrapy.Request(_url, callback=self.get_sub_conf, meta = meta)
          yield req

        """

        req = scrapy.Request(_url, callback=self.get_sub_conf, meta = meta)
        yield req

        #"""

    def get_sub_conf(self, response):
      meta = response.meta
      for l in self.lx_sections.extract_links(response): 
        _url = l.url                  
        yield scrapy.Request(_url, self.get_speaker_link, meta = meta)

    def get_speaker_link(self, response):
      meta = response.meta
      for l in self.lx_speakers.extract_links(response): 
        _url = l.url                  
        yield scrapy.Request(_url, self.parse_speakers, meta = meta)

    def parse_speakers(self, response):
      meta = response.meta

      speaker_name = response.xpath('.//h4[@class="gdlr-speaker-name"]//text()').extract_first()
      if speaker_name:
        speaker_name = string.capwords(speaker_name).encode('utf-8')
      else:
        return

      speaker_position = response.xpath('.//div[@class="gdlr-speaker-position"]//text()').extract_first()
      company = ''
      speaker_role = ''
      if speaker_position:
        speaker_position_parts = speaker_position.split(' at ')
        if len(speaker_position_parts) < 2:
          speaker_position_parts = speaker_position.split(',')
        
        if len(speaker_position_parts) > 1:
          speaker_role = speaker_position_parts[0].strip()
          company = speaker_position_parts[1].strip()
        else:
          speaker_role = speaker_position

      bio = ''
      bios= response.xpath('.//div[@class="gdlr-speaker-content"]//text()').extract()
      if bios:        
        aBio = [x.strip() for x in bios if x]
        newBio = []
        for t in aBio:
          if t and t[-1] == '.':
            t = t + '\n'

          if t:
            newBio.append(t)

        bio = ' '.join(newBio).encode('utf-8')     

      speaker_photo = response.xpath('.//div[@class="gdlr-speaker-thumbnail"]//a//@href').extract_first()
      
      name_parts = speaker_name.split(' ')
      firstname = ' '.join(name_parts[:-1])
      lastname = name_parts[-1]

      """ Insert to table 'INDIVIDUALS' """
      indiv_fields = {
        'F. Name': firstname,
        'L. Name': lastname,                
        'Biography': bio if bio else '',    
        'Individual Link': response.url,
        'Link to Conf': [meta['conf']['id']]
      }

      if speaker_photo:
        indiv_fields.update({
            'Pic': [{'url': speaker_photo}],
          })

      search_speaker_name = speaker_name.replace(u"\u2019", "'").encode('utf-8').replace("'", r"\'")

      indivs = self._air_indiv_table.search('Name', str(search_speaker_name))

      if not indivs:                            
        indiv = self._air_indiv_table.insert(indiv_fields)
        indiv_id = indiv['id']
      else:
        # Update person
        indiv_id = indivs[0]['id']  
        update = self._air_indiv_table.update(indiv_id, indiv_fields)
      """ End Insert to table 'INDIVIDUALS' """

      """ Insert person to 'INDIVIDUAL ROLE AT PANEL' """
      if 'panel_id' in meta.keys():
        panel_id = meta['panel_id']
        role_at_panel_fields = {
              'Link to Panel': [panel_id],
              'Link to Person': [indiv_id],
              'Role': ['Speaker']
            }

        if ',' in search_speaker_name:
          _person_name = '"' + search_speaker_name + '"'
        else:
          _person_name = search_speaker_name
        indiv_role_at_panel_s = self._air_role_panel_table.search('Link to Person', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_panel_s):          
          for i in indiv_role_at_panel_s:
            print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
            if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
              self._air_role_panel_table.update(i['id'], role_at_panel_fields)
              iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT PANEL'
          inserted = self._air_role_panel_table.insert(role_at_panel_fields)

      """ End Insert person to 'INDIVIDUAL ROLE AT PANEL' """

      """ Insert 'COMPANIES' """  

      if company != '':    
        inserted_companies = {}
        for x in self.all_companies:
          if 'Name' in x['fields'].keys():
            company_name = x['fields']['Name']

            clean_company_name = self.clean_company(company_name)
            company_id = x['id']   
            inserted_companies[clean_company_name] = company_id        

        inserted_companies_keys = inserted_companies.keys()    
        
        insert_com_fields = {
          'Name': company.strip()
        }
        clean_company_name = self.clean_company(company)

        if clean_company_name in inserted_companies_keys:
          company_id = inserted_companies[clean_company_name]
        else:
          # Insert company
          inserted_company = self._air_company_table.insert(insert_com_fields)
          company_id = inserted_company['id'] 
          self.all_companies.append(inserted_company)           

      """ End Insert Company """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
      if company and speaker_role:
        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': ['Employee'],
          'Official Title': speaker_role
        }

        _person_name = str(search_speaker_name)
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'
        else:
          _person_name = search_speaker_name

        indiv_role_at_company_s = self._air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            if 'Link to Company' in i['fields'].keys():
              print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
              if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = self._air_role_at_company_table.insert(role_at_company_fields)

        """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M%p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
