# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 
import hashlib

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable
from scrapy.linkextractors import LinkExtractor

class SponsorSpider(scrapy.Spider):
    name = "sponsor"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(SponsorSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        #self.allowed_domains = ['*']

        #domain = get_domain(start_url)
        #self.allowed_domains.append(domain)
        #self.allowed_domains.append('ai-expo.net')
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)
        self._air_company_role_at_conf_table = Airtable(self.base_key, 'COMPANY ROLE AT CONF', api_key=self.api_key)

        # Get all companies
        self.all_companies = self._air_company_table.get_all()
        #self.all_companies = []

        self.inserted_companies = {}
        for x in self.all_companies:
          if 'Name' in x['fields'].keys():
            company_name = x['fields']['Name']

            clean_company_name = self.clean_company(company_name)
            company_id = x['id']   
            self.inserted_companies[clean_company_name] = company_id

        self.inserted_companies_keys = self.inserted_companies.keys()

        self._air_company_role_at_conf_table = Airtable(self.base_key, 'COMPANY ROLE AT CONF', api_key=self.api_key)
        company_role_at_conf = self._air_company_role_at_conf_table.get_all()

        self.inserted_role = {}

        for role in company_role_at_conf:
          #inserted_role[role['fields']['Link to Company'][0]] = role['id']
          if ('Link to Company' in role['fields'].keys()) and ('Link to Conference' in role['fields'].keys()):
            conf = role['fields']['Link to Conference'][0]
            com_id = role['fields']['Link to Company'][0]
            _string = ''.join([conf, com_id])

            unique_id = self.md5hash(_string)

            if unique_id not in self.inserted_role.keys():
              self.inserted_role[unique_id] = [role]
            else:
              self.inserted_role[unique_id].append(role)

        self.iCnt = 0

        self.lx_sections = LinkExtractor(restrict_xpaths=(),
                                allow=(r'/\#sponsors'), deny=())

        self.lx_partners = LinkExtractor(restrict_xpaths=(),
                                allow=(r'/sponsor/'), deny=())

    def parse(self, response):      
      
      search_term = 'Token Fest (Boston, 2018)'
      confs = self._air_confer_table.search('ID', str(search_term))
      
      for x in confs:                
        _url = x['fields']['URL']   
        meta = {
          'conf': x,
          'original_url': _url
        }
        """
        if 'blockchain-expo.com/northamerica/' in _url:
          req = scrapy.Request(_url, callback=self.get_sponsor_link, meta = meta)
          yield req

        """

        req = scrapy.Request(_url, callback=self.get_partner_link, meta = meta)
        yield req

        #"""

    def get_sponsor_link(self, response):
      meta = response.meta
      for l in self.lx_sections.extract_links(response): 
        _url = l.url                  
        yield scrapy.Request(_url, self.get_partner_link, meta = meta) 

    def get_partner_link(self, response):
      meta = response.meta

      levels = response.xpath('.//div[@class="sponsors-sections-wrapper"]//div[@class="sponsors-section"]')

      for i, level in enumerate(levels):        
        tier = 'Tier ' + str(i + 1)
        sponsors = level.xpath('.//div[@class="sponsors-images"]//a[contains(@class, "sponsor-item")]')
        for sponsor_ in sponsors:
          sponsor_url = sponsor_.xpath('.//@href').extract_first()
          sponsor_image = sponsor_.xpath('.//img//@src').extract_first()
          sponsor_name = sponsor_.xpath('.//@title').extract_first()
          
          company_name = sponsor_name
          if not company_name:
            return

          company_url = sponsor_url          
          company_logo = sponsor_image        

          insert_com_fields = {
            'Name': str(company_name).strip()         
          }

          if company_url:
            insert_com_fields.update({'URL': str(company_url)})

          if company_logo:        
            insert_com_fields.update({
                'Logo': [{'url': company_logo}]
              })
          
          clean_company_name = self.clean_company(company_name)

          if clean_company_name in self.inserted_companies_keys:
            print "Updating"
            #print company_id, insert_com_fields
            company_id = self.inserted_companies[clean_company_name]
            # Update company info          
            self._air_company_table.update(company_id, insert_com_fields)
          else:
            # Insert company
            print "Inserting"            
            inserted_company = self._air_company_table.insert(insert_com_fields)
            self.all_companies.append(inserted_company)
            company_id = inserted_company['id']
            self.inserted_companies[clean_company_name] = company_id
            self.inserted_companies_keys.append(clean_company_name)            
            
          """ End Insert company """

          """ Insert company role at conf """      
          #company_role_at_conf = self._air_company_role_at_conf_table.search('Link to Conference', '"' + meta['conf']['fields']['ID'] + '"')
          company_role_at_conf = self._air_company_role_at_conf_table.search('Link to Conference', '"' + str(meta['conf']['fields']['ID']) + '"')
          
          inserted_role = {}

          for role in company_role_at_conf:
            inserted_role[role['fields']['Link to Company'][0]] = [role['id'], role['fields']['Role']]

          _role = ["Sponsor"]      
          _unique_id = self.md5hash(''.join([meta['conf']['id'], company_id]))

          if _unique_id in self.inserted_role.keys():
            bExist = False
            for r in self.inserted_role[_unique_id]:
              if 'Role' not in r['fields'].keys():
                continue

              if "Sponsor" in r['fields']['Role']:
                bExist = True
                break

            if bExist == False:
              print "Updating sponsor........."
              _id = self.inserted_role[_unique_id][0]['id']
              old_role = self.inserted_role[_unique_id][0]['fields']['Role']
              for _r in old_role:
                _role.append(_r)
              # Update
              new_role = list(set(_role))
              role_fields = {
                'Role': new_role,
                'Sponsorship Level': tier
                }
              _updated = self._air_company_role_at_conf_table.update(_id, role_fields)
              self.inserted_role[_unique_id] = [_updated].extend(self.inserted_role[_unique_id][1:])
          else:
            # Insert
            print "Inserting sponsor.........."
            role_fields = {
              'Link to Company': [company_id],
              'Link to Conference': [meta['conf']['id']],
              'Role': _role,
              'Sponsorship Level': tier
              }
            _insert = self._air_company_role_at_conf_table.insert(role_fields)
            self.inserted_role[_unique_id] = [_insert]

          """ End Insert company role at conf """

    def md5hash(self, _string):        
      _hex = hashlib.md5(_string.encode('utf-8')).hexdigest()
      return _hex

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M%p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
