# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class BackendSpider(scrapy.Spider):
    name = "backend"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_confer_table = None
    conference = None
    conference_search = None
    sponsor_id = None
    confs = []
    iCnt = 0

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(BackendSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    
        
        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self.confs = self._air_confer_table.get_all()
        print len(self.confs)
        for x in self.confs:
          if 'URL' in x['fields'].keys():
            url = x['fields']['URL']
            domain = get_domain(url)
            self.allowed_domains.append(domain)


    def parse(self, response):                  
      for x in self.confs:
        if 'URL' in x['fields'].keys():
          url = x['fields']['URL']
          conf_id = x['id']          
          meta = {
            'conf_id': conf_id,
            'conf': x
          }
          try:
            req = scrapy.Request(url, callback=self.parse_details, meta=meta)
            yield req
          except:
            pass

    def parse_details(self, response):
      meta = response.meta
      url = response.url
      domain = get_domain(url)
      if domain == self.config['domain']:
        update_fields = {
          'Backend': ['Keynote']
        }

        self._air_confer_table.update(meta['conf_id'], update_fields)
        self.iCnt += 1
        print self.iCnt

    def compare_2_strings(self, string1, string2):
      list1 = self.split_string(string1)
      list2 = self.split_string(string2)

      result = list(set(list1) & set(list2))
      #print result

      if len(result) >= len(list1)/2:
        return True
      return False

    def split_string(self, _string):
      _string = _string.lower().replace(',', ' ').replace(':', ' ')\
        .replace('-', ' ').replace('&', ' ').replace('?', ' ').replace('.', ' ')
      _words = [x for x in _string.split(' ') if x.strip()]
      return _words

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
