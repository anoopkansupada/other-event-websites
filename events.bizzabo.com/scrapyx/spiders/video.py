# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class VideoSpider(scrapy.Spider):
    name = "video"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_panels_table = None
    conference = None
    conference_search = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(VideoSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
                    
        self.allowed_domains.append('cdn.jwplayer.com')

        if url != None:
          start_url = url
        else:
          start_url = self.config['video_url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)    

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    
        
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)

    def parse(self, response):            
      _air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)

      script_text = response.xpath('.//script[contains(text(), "local-start-date")]//text()').extract_first()

      matches = re.findall('.*&quot;eventId&quot;\:&quot;(.*?)&quot;.*', script_text)
      if matches:                        
        self.event_id = matches[0]
      else:
        googleplay = response.xpath('.//meta[@name="twitter:app:url:googleplay"]//@content').extract_first()
        self.event_id = googleplay.split('/')[-1]


      matches0 = re.findall('.*&quot;local-end-date&quot;\:&quot;(.*?)&quot;,&quot;end-date.*', script_text)
      if matches0:                        
        conf_end_date = datetime.datetime.strptime(matches0[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')        

      matches1 = re.findall('.*&quot;local-start-date&quot;\:&quot;(.*?)&quot;,&quot;start-date.*', script_text)
      if matches1:        
        conf_start_date = datetime.datetime.strptime(matches1[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')                
      
      matches2 = re.findall('.*&quot;,&quot;time-zone-id&quot;\:&quot;(.*?)&quot;,&quot;unique-name.*', script_text)
      if matches2:
        tz = matches2[0]     
        print tz   

      city = ''
      matches31 = re.findall('.*&quot;,&quot;city&quot;\:&quot;(.*?)&quot;,&quot.*', script_text)
      if matches31:          
        city = matches31[0]          

      matches3 = re.findall('.*&quot;,&quot;country&quot;\:&quot;(.*?)&quot;,&quot;display-address&quot;\:&quot;(.*?)&quot;,&quot;latitude.*', script_text)
      if matches3:                    
        country = matches3[0][0]
        address = matches3[0][1]             

      matches4 = re.findall('.*&quot;name&quot;\:&quot;(.*?)&quot;,&quot;privacy.*', script_text)
      if matches4:
        conference_name = matches4[0]

      conference_desc = ''
      matches5 = re.findall('.*&quot;description&quot;\:&quot;(.*?)&quot;,&quot;download-app-links.*', script_text)
      if matches5:
        conference_desc = matches5[0]

      matches6 = re.findall('.*&quot;agendaUrl&quot;\:&quot;(.*?)&quot;,&quot;associatedOnly.*', script_text)
      if matches6:
        agendaUrl = matches6[0]

      matches7 = re.findall('.*&quot;homepage-url&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
      if matches7:
        conference_url = matches7[0]

      conf = []
      confs = _air_confer_table.search('Name', str(conference_name))
      #print conf
      conf_fields = {
          'Notes': conference_desc,
          'Name': conference_name,
          'Year': conf_start_date.split('/')[-1],
          'Start Date': conf_start_date,
          'End Date': conf_end_date,
          'URL': conference_url,
          'Agenda/People URL': agendaUrl,
          'City': city,
          'Country': country
        }

      #print conf_fields
      #self._exit()

      if not confs:  
        pass      
        #conf = _air_confer_table.insert(conf_fields)
      else:
        conf = confs[0]        

      self.conf_ID = conf['fields']['ID']
      self.conf_idx = conf['id']

      self.conference_search = str(conference_name + ' (' + city + ', ' + conf_start_date.split('/')[-1] + ')')      
      self.conference_search = '"' + self.conference_search + '"'

      # Parse videos
      for div in response.xpath('.//div[contains(@id, "video-player-")]'):
        video_player = div.xpath('.//@id').extract_first()
        player_id = video_player.split('-')[-1]
        video_player_api = self.config['video_player_api'].format(player_id=player_id)
        req = scrapy.Request(video_player_api, callback=self.parse_videos)
        yield req  

    def parse_videos(self, response):
      json_data = json.loads(response.body)   
      if 'playlist' in json_data.keys():
        playlist = json_data['playlist']

        panels = self._air_panels_table.search('Link to Conference', self.conference_search)
        print len(panels)

        panel_index = {}
        for x in panels:
          panel_index[x['fields']['Topic']] = x['id']

        panel_titles = panel_index.keys()
        remain_list = []
        for x in playlist:
          title = x['title']
          panel_title = title.replace('- ' + self.conference, '').strip().encode('utf-8')

          bFound = False
          for p in panel_titles:
            key = p
            if panel_title.lower() in p.lower():
              
              video_file = ''
              for v in x['sources']:
                if 'label' in v.keys():
                  if v['label'] == '720p':
                    video_file = v['file']

              if video_file != '':
                panel_id = panel_index[key]
                update_fields = {
                  'Video': video_file,
                  'Video Title': title.encode('utf-8')
                }

                self._air_panels_table.update(panel_id, update_fields)
                bFound = True
                panel_index.pop(key)
                break
          if not bFound:
            remain_list.append(x)

      #print remain_list
      
      remain_list2 = []
      remain_titles = []
      for x in remain_list:
        panel_titles2 = panel_index.keys()
        title = x['title']
        panel_title = title.replace('- ' + self.conference, '').strip().encode('utf-8')

        bFound = False
        for p in panel_titles2:
          key = p
          result = self.compare_2_strings(panel_title, p)
          if result:            
            video_file = ''
            for v in x['sources']:
              if 'label' in v.keys():
                if v['label'] == '720p':
                  video_file = v['file']

            if video_file != '':
              panel_id = panel_index[key]
              update_fields = {
                'Video': video_file,
                'Video Title': title.encode('utf-8')
              }

              self._air_panels_table.update(panel_id, update_fields)
              bFound = True
              panel_index.pop(key)
              break
        if not bFound:
          remain_list2.append(x)
          remain_titles.append(title)

      print remain_titles


    def compare_2_strings(self, string1, string2):
      list1 = self.split_string(string1)
      list2 = self.split_string(string2)

      result = list(set(list1) & set(list2))
      #print result

      if len(result) >= len(list1)/2:
        return True
      return False

    def split_string(self, _string):
      _string = _string.lower().replace(',', ' ').replace(':', ' ')\
        .replace('-', ' ').replace('&', ' ').replace('?', ' ').replace('.', ' ')
      _words = [x for x in _string.split(' ') if x.strip()]
      return _words

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
