# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class SpeakerSpider(scrapy.Spider):
    name = "speaker"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(SpeakerSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
                    
        self.allowed_domains.append('api.bizzabo.com')

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    

        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self.bizzabo_confs = self._air_confer_table.search('Backend', 'Bizzabo')

        for x in self.bizzabo_confs:       
          if 'Speakers URL' in x['fields'].keys():
            _url = x['fields']['Speakers URL']   
            if _url:
              domain = get_domain(_url)
              self.allowed_domains.append(domain)

    def parse(self, response):                   
      for x in self.bizzabo_confs:       
        if 'Speakers URL' in x['fields'].keys():
          _url = x['fields']['Speakers URL']   
          if _url:            
            meta = {
              'conf': x,
              'original_url': _url
            }

            req = scrapy.Request(_url, callback=self.parse2, meta = meta)
            yield req

    def parse2(self, response):      
      meta = response.meta
      # Add to table 'CONFERENCE'      

      script_text = response.xpath('.//script[contains(text(), "local-start-date")]//text()').extract_first()
      if not script_text:
        return

      matches = re.findall('.*&quot;eventId&quot;\:&quot;(.*?)&quot;.*', script_text)
      if matches:                        
        event_id = matches[0]
      else:
        googleplay = response.xpath('.//meta[@name="twitter:app:url:googleplay"]//@content').extract_first()
        event_id = googleplay.split('/')[-1]


      matches0 = re.findall('.*&quot;local-end-date&quot;\:&quot;(.*?)&quot;,&quot;end-date.*', script_text)
      if matches0:                        
        conf_end_date = datetime.datetime.strptime(matches0[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')        

      matches1 = re.findall('.*&quot;local-start-date&quot;\:&quot;(.*?)&quot;,&quot;start-date.*', script_text)
      if matches1:        
        conf_start_date = datetime.datetime.strptime(matches1[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')                
      
      matches2 = re.findall('.*&quot;,&quot;time-zone-id&quot;\:&quot;(.*?)&quot;,&quot;unique-name.*', script_text)
      if matches2:
        tz = matches2[0]     
        print tz   

      city = ''
      matches31 = re.findall('.*&quot;city&quot;\:&quot;(.*?)&quot;,&quot.*', script_text)
      if matches31:          
        city = matches31[0]          

      matches3 = re.findall('.*&quot;,&quot;country&quot;\:&quot;(.*?)&quot;,&quot;display-address&quot;\:&quot;(.*?)&quot;,&quot;latitude.*', script_text)
      if matches3:                    
        country = matches3[0][0]
        address = matches3[0][1]      
      else:    
        matches3 = re.findall('.*&quot;country&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
        if matches3:                    
          country = matches3[0]        

      matches4 = re.findall('.*&quot;name&quot;\:&quot;(.*?)&quot;,&quot;privacy.*', script_text)
      if matches4:
        conference_name = matches4[0]

      conference_desc = ''
      matches5 = re.findall('.*&quot;description&quot;\:&quot;(.*?)&quot;,&quot;download-app-links.*', script_text)
      if matches5:
        conference_desc = matches5[0]

      matches6 = re.findall('.*&quot;agendaUrl&quot;\:&quot;(.*?)&quot;,&quot;associatedOnly.*', script_text)
      if matches6:
        agendaUrl = matches6[0]

      matches7 = re.findall('.*&quot;homepage-url&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
      if matches7:
        conference_url = matches7[0]

      """
      conf = []
      confs = _air_confer_table.search('Name', str(conference_name))
      #print conf
      conf_fields = {
          'Notes': conference_desc,
          'Name': conference_name,
          'Year': conf_start_date.split('/')[-1],
          'Start Date': conf_start_date,
          'End Date': conf_end_date,
          'URL': conference_url,
          'Agenda/People URL': agendaUrl,
          'City': city,
          'Country': country
        }

      #print conf_fields
      #self._exit()

      if not confs:        
        pass
        #conf = _air_confer_table.insert(conf_fields)
      else:
        conf = confs[0]        
      """
      conf_ID = meta['conf']['fields']['ID']
      conf_idx = meta['conf']['id']

      meta = {
        'event_id': event_id,         
        'conf_ID': conf_ID,
        'conf_idx': conf_idx
      }

      # Parse speakers
      speakers_api = self.config['speakers_api'].format(event_id=event_id)
      req = scrapy.Request(speakers_api, callback=self.parse_speakers, meta=meta)
      yield req  

    def parse_speakers(self, response):
      meta = response.meta
      json_data = json.loads(response.body)
      for x in json_data:
        speaker_detail_api = self.config['speaker_detail_api'].format(event_id=meta['event_id'], speaker_id=x['id'])
        req = scrapy.Request(speaker_detail_api, callback=self.parse_speaker_details, meta = meta)
        yield req

    def parse_speaker_details(self, response):
      meta = response.meta
      json_data = json.loads(response.body)   
      if 'email' in json_data.keys():
        indiv_fields = {
          'Email': json_data['email']
        }      

        individual_link = self.config['speaker_url'].format(event_id=meta['event_id'], speaker_id=json_data['id'])

        indivs = self._air_indiv_table.search('Individual Link', str(individual_link))
        if indivs:                                      
          # Update person
          indiv_id = indivs[0]['id']              
          update = self._air_indiv_table.update(indiv_id, indiv_fields)


    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
