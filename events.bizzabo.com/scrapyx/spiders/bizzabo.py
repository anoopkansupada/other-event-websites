# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class BizzaboSpider(scrapy.Spider):
    name = "bizzabo"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    #event_id = None
    #sessions_list = []
    #speakers_list = {}
    #inserted_speakers_list = {}
    #locations = {}
    #filters = {}

    #conf_ID = None
    #conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None
    _air_company_table = None
    handle_httpstatus_list = [404] 

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(BizzaboSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
                    
        self.allowed_domains.append('bizzabo.com')

        """
        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        
        """

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    

        #self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        #self.all_companies = self._air_company_table.get_all()

        #self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        #self.bizzabo_confs = self._air_confer_table.search('Backend', 'Bizzabo')
        #print len(self.bizzabo_confs)
        #sys.exit()

        id_range = range(207175, 207177)
        for _id in id_range:
          event_url = 'https://events.bizzabo.com/' + str(_id)
          self.start_urls.append(event_url)

    def parse(self, response):                   
      if response.status == 404:
        return

      script_text = response.xpath('.//script[@type="application/ld+json"]').extract_first().replace('\n', '')

      name = ''
      description = ''
      names = re.findall('.*?"name": "(.*?)".*?"startDate".*?', script_text)
      if names:
        name = names[0]

      descriptions = re.findall('.*?"description": "(.*?)".*?"endDate".*?', script_text)
      if descriptions:
        description = descriptions[0]

      print (name, description)
      if self.check_topic(name) or self.check_topic(description):
        print '---------This event related to crypto---------'

    def check_topic(self, _string):
      if not _string:
        return False
      keywords = ["blockchain", "ethereum", "bitcoin", \
        "litecoin", "virtual currency", 'crypto', 'cryptocurrency']
      iCnt = 0
      _string = _string.lower()
      for x in keywords:
        if x in _string:
          iCnt += 1

      if iCnt > 0:
        return True

      return False

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
