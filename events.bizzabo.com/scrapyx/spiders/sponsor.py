# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class SponsorSpider(scrapy.Spider):
    name = "sponsor"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_panels_table = None
    conference = None
    conference_search = None
    sponsor_id = None
    original_companies = {}
    bizzabo_companies = {}

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(SponsorSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
                    
        self.allowed_domains.append('bizzabo.com')

        if url != None:
          start_url = url
        else:
          start_url = self.config['sponsors_url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    
        
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self.all_companies = self._air_company_table.get_all()
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self.bizzabo_confs = self._air_confer_table.search('Backend', 'Bizzabo')

        for x in self.bizzabo_confs:       
          if 'Sponsor URL' in x['fields'].keys():
            _url = x['fields']['Sponsor URL']   
            if _url:
              domain = get_domain(_url)
              self.allowed_domains.append(domain)

    def parse(self, response):                   
      for x in self.bizzabo_confs:       
        if 'Sponsor URL' in x['fields'].keys():
          _url = x['fields']['Sponsor URL']   
          if _url:            
            meta = {
              'conf': x,
              'original_url': _url
            }

            req = scrapy.Request(_url, callback=self.parse2, meta = meta)
            yield req

    def parse2(self, response):     
      meta = response.meta       
      #_air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)

      script_text = response.xpath('.//script[contains(text(), "local-start-date")]//text()').extract_first()
      if not script_text:
        return

      matches = re.findall('.*&quot;eventId&quot;\:&quot;(.*?)&quot;.*', script_text)
      if matches:                        
        event_id = matches[0]
      else:
        googleplay = response.xpath('.//meta[@name="twitter:app:url:googleplay"]//@content').extract_first()
        event_id = googleplay.split('/')[-1]


      matches0 = re.findall('.*&quot;local-end-date&quot;\:&quot;(.*?)&quot;,&quot;end-date.*', script_text)
      if matches0:                        
        conf_end_date = datetime.datetime.strptime(matches0[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')        

      matches1 = re.findall('.*&quot;local-start-date&quot;\:&quot;(.*?)&quot;,&quot;start-date.*', script_text)
      if matches1:        
        conf_start_date = datetime.datetime.strptime(matches1[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')                
      
      matches2 = re.findall('.*&quot;,&quot;time-zone-id&quot;\:&quot;(.*?)&quot;,&quot;unique-name.*', script_text)
      if matches2:
        tz = matches2[0]     
        print tz   

      city = ''
      matches31 = re.findall('.*&quot;city&quot;\:&quot;(.*?)&quot;,&quot.*', script_text)
      if matches31:          
        city = matches31[0]          

      matches3 = re.findall('.*&quot;,&quot;country&quot;\:&quot;(.*?)&quot;,&quot;display-address&quot;\:&quot;(.*?)&quot;,&quot;latitude.*', script_text)
      if matches3:                    
        country = matches3[0][0]
        address = matches3[0][1]     
      else:    
        matches3 = re.findall('.*&quot;country&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
        if matches3:                    
          country = matches3[0]         

      matches4 = re.findall('.*&quot;name&quot;\:&quot;(.*?)&quot;,&quot;privacy.*', script_text)
      if matches4:
        conference_name = matches4[0]

      conference_desc = ''
      matches5 = re.findall('.*&quot;description&quot;\:&quot;(.*?)&quot;,&quot;download-app-links.*', script_text)
      if matches5:
        conference_desc = matches5[0]

      matches6 = re.findall('.*&quot;agendaUrl&quot;\:&quot;(.*?)&quot;,&quot;associatedOnly.*', script_text)
      if matches6:
        agendaUrl = matches6[0]

      matches7 = re.findall('.*&quot;homepage-url&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
      if matches7:
        conference_url = matches7[0]

      matches8 = re.findall('.*&quot;id&quot;\:(.*?),&quot;type&quot;\:&quot;sponsors&quot;.*', script_text)
      if matches8:
        sponsor_id = matches8[0] 
        #print self.sponsor_id    
      else:
        script_text = response.xpath('.//script[contains(@id, "bootstrap-data-siteConf")]//text()').extract_first()
        matches8 = re.findall('.*&quot;id&quot;\:(.*?),&quot;type&quot;\:&quot;home.*', script_text)
        if matches8:
          sponsor_id = matches8[0] 
          #print self.sponsor_id

      """
      conf = []
      confs = _air_confer_table.search('Name', str(conference_name))
      #print conf
      conf_fields = {
          'Notes': conference_desc,
          'Name': conference_name,
          'Year': conf_start_date.split('/')[-1],
          'Start Date': conf_start_date,
          'End Date': conf_end_date,
          'URL': conference_url,
          'Agenda/People URL': agendaUrl,
          'City': city,
          'Country': country
        }

      print conf_fields
      #self._exit()

      if not confs:  
        pass      
        #conf = _air_confer_table.insert(conf_fields)
      else:
        conf = confs[0]               

      self.conference_search = str(conference_name + ' (' + city + ', ' + conf_start_date.split('/')[-1] + ')')      
      self.conference_search = '"' + self.conference_search + '"'

      print self.conf_ID
      """

      # Parse Sponsors 
      """
      text = response.xpath('.//div[@class="container"]//text()').extract()
      print text

      matches9 = re.findall('.*href="(.*?)" alt="(.*?)" style="background-image:url(&quot;(.*?)&quot;);"', text)
      if matches9:
        print matches9
      """

      conf_ID = meta['conf']['fields']['ID']
      conf_idx = meta['conf']['id']    
      original_companies = {}

      for a in response.xpath('.//div[contains(@class, "sponsor-row")]//a'):
        company_url = a.xpath('.//@href').extract_first()
        company_name = a.xpath('.//@alt').extract_first()

        company_logo = ''
        style = a.xpath('.//@style').extract_first()
        #print style
        matches = re.findall('^background-image\:url\("(.*?)"\);$', style)
        if matches:
          company_logo = matches[0]

        company = {
          'company_url': company_url,
          'company_name': company_name          
        }

        if company_logo != '':
          company.update({
              'company_logo': str(company_logo).replace("///", "//")
            })

        #print company

        original_companies[company_name] = company

      meta = {
        'event_id': event_id,         
        'conf_ID': conf_ID,
        'conf_idx': conf_idx,
        'original_companies': original_companies
      }

      #print self.original_companies

      # Parse videos
      bizzabo_sponsors_url = self.config['bizzabo_sponsors_url'].format(event_id=event_id, sponsor_id=sponsor_id)
      req = scrapy.Request(bizzabo_sponsors_url, callback=self.parse_sponsorss, meta = meta)
      yield req  

    def parse_sponsorss(self, response):
      meta = response.meta
      bizzabo_companies = {}
      for div in response.xpath('.//div[contains(@class, "sponsor-row-holder")]'):
        if div.xpath('.//div[contains(@class, "sponsor-img-holder")]'):
            company_name = div.xpath('.//div[contains(@class, "sponsor-img-holder")]//@title').extract_first()

            desc = div.xpath('.//div[contains(@class, "sponsor-img-holder")]//@data-content').extract_first()
            sel = Selector(text=desc, type = "html")
            company_desc = sel.xpath('.//div[contains(@class, "atom-main")]//text()').extract_first()            
            company_logo = div.xpath('.//div[contains(@class, "sponsor-img-holder")]//img//@src').extract_first()
            company_url = div.xpath('.//div[contains(@class, "sponsor-img-holder")]//a[1]//@href').extract_first()

            _company = {
              'company_name': company_name,
              'company_desc': company_desc.encode('utf-8'),              
              'company_url': company_url if company_url else '',
            }

            if company_logo:
              _company.update({
                  'company_logo': str(company_logo)
                })

            bizzabo_companies[company_name] = _company

      #print self.bizzabo_companies
      #self._exit()
      original_companies = meta['original_companies']
      if len(original_companies):
        new_original_companies = {}
        for x in original_companies.keys():
          for y in bizzabo_companies.keys():
            original_name = self.clean_company(x)
            bizzabo_name = self.clean_company(y)
            if y == x:
              original_companies[x].update({
                'company_desc': bizzabo_companies[y]['company_desc']
                })          
              break
      else:
        original_companies = bizzabo_companies

      """ Insert company """
      #_air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
      #all_companies = _air_company_table.get_all()

      inserted_companies = {}
      for x in self.all_companies:
        if 'Name' in x['fields'].keys():
          company_name = x['fields']['Name']

          clean_company_name = self.clean_company(company_name)
          company_id = x['id']   
          inserted_companies[clean_company_name] = company_id

      inserted_companies_keys = inserted_companies.keys()

      company_id_list = []
      for c in original_companies.values():
        company_name =c['company_name']
        insert_com_fields = {
          'Name': str(company_name),
          'URL': str(c['company_url']),
          'Company Description': str(c['company_desc']) if 'company_desc' in c.keys() else ''          
        }

        if 'company_logo' in c.keys():
          insert_com_fields.update({
              'Logo': [{'url': c['company_logo']}]
            })
        
        clean_company_name = self.clean_company(company_name)

        if clean_company_name in inserted_companies_keys:
          print "Updating"
          #print company_id, insert_com_fields
          company_id = inserted_companies[clean_company_name]
          # Update company info          
          self._air_company_table.update(company_id, insert_com_fields)
        else:
          # Insert company
          print "Inserting"
          inserted_company = self._air_company_table.insert(insert_com_fields)
          self.all_companies.append(inserted_company)
          company_id = inserted_company['id']
          inserted_companies[clean_company_name] = company_id
          inserted_companies_keys.append(clean_company_name)

        #c.update({'company_id': company_id})
        company_id_list.append(company_id)
        
      """ End Insert company """

      """ Insert company role at conf """
      _air_company_role_at_conf_table = Airtable(self.base_key, 'COMPANY ROLE AT CONF', api_key=self.api_key)
      company_role_at_conf = _air_company_role_at_conf_table.search('Link to Conference', '"' + meta['conf_ID'] + '"')

      inserted_role = {}

      for role in company_role_at_conf:
        inserted_role[role['fields']['Link to Company'][0]] = role['id']

      for x in company_id_list:                

        role_fields = {
          'Role': ["Sponsor"]
        }

        if x in inserted_role.keys():
          _id = inserted_role[x]
          # Update
          #_air_company_role_at_conf_table.update(_id, role_fields)
        else:
          # Insert
          role_fields.update({
            'Link to Company': [x],
            'Link to Conference': [meta['conf_idx']]
            })
          _air_company_role_at_conf_table.insert(role_fields)

      """ End Insert company role at conf """

    def compare_2_strings(self, string1, string2):
      list1 = self.split_string(string1)
      list2 = self.split_string(string2)

      result = list(set(list1) & set(list2))
      #print result

      if len(result) >= len(list1)/2:
        return True
      return False

    def split_string(self, _string):
      _string = _string.lower().replace(',', ' ').replace(':', ' ')\
        .replace('-', ' ').replace('&', ' ').replace('?', ' ').replace('.', ' ')
      _words = [x for x in _string.split(' ') if x.strip()]
      return _words

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
