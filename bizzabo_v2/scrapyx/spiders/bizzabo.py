# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class BizzaboSpider(scrapy.Spider):
    name = "bizzabo"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    #event_id = None
    #sessions_list = []
    #speakers_list = {}
    #inserted_speakers_list = {}
    #locations = {}
    #filters = {}

    #conf_ID = None
    #conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None
    _air_company_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(BizzaboSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]
                    
        self.allowed_domains.append('api.bizzabo.com')

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass    

        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self.all_companies = self._air_company_table.get_all()

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self.bizzabo_confs = self._air_confer_table.search('Backend', 'Bizzabo')
        #print len(self.bizzabo_confs)
        #sys.exit()

        for x in self.bizzabo_confs:       
          if 'Agenda/People URL' in x['fields'].keys():
            _url = x['fields']['Agenda/People URL']   
            if _url:
              domain = get_domain(_url)
              self.allowed_domains.append(domain)

    def parse(self, response):                   
      for x in self.bizzabo_confs:       
        if 'Agenda/People URL' in x['fields'].keys():
          _url = x['fields']['Agenda/People URL']   
          if _url:            
            meta = {
              'conf': x,
              'original_url': _url
            }

            if _url == 'https://events.bizzabo.com/cryptoinvevstsummit/agenda':
              req = scrapy.Request(_url, callback=self.parse_agenda, meta = meta)
              yield req
            
            """
            req = scrapy.Request(_url, callback=self.parse_agenda, meta = meta)
            yield req
            """

    def parse_agenda(self, response):      
      # Add to table 'CONFERENCE'      
      meta = response.meta
      event_id = None
      script_text = response.xpath('.//script[contains(text(), "local-start-date")]//text()').extract_first()
      if script_text:        
        matches = re.findall('.*&quot;eventId&quot;\:&quot;(.*?)&quot;.*', script_text)
        if matches:                        
          event_id = matches[0]

      if not event_id:
        googleplay = response.xpath('.//meta[@name="twitter:app:url:googleplay"]//@content').extract_first()
        if googleplay and 'bizzabo' in googleplay:
          event_id = googleplay.split('/')[-1]

      if not event_id:
        event_id = response.xpath('.//div[@name="bizzabo-web-agenda"]//@data-unique-name').extract_first()

      print event_id

      if not event_id:
        return

      """
      matches0 = re.findall('.*&quot;local-end-date&quot;\:&quot;(.*?)&quot;,&quot;end-date.*', script_text)
      if matches0:                        
        conf_end_date = datetime.datetime.strptime(matches0[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')        

      matches1 = re.findall('.*&quot;local-start-date&quot;\:&quot;(.*?)&quot;,&quot;start-date.*', script_text)
      if matches1:        
        conf_start_date = datetime.datetime.strptime(matches1[0].split(' ')[0], '%Y-%m-%d').strftime('%m/%d/%Y')                
      
      matches2 = re.findall('.*&quot;,&quot;time-zone-id&quot;\:&quot;(.*?)&quot;,&quot;unique-name.*', script_text)
      if matches2:
        tz = matches2[0]     
        print tz   

      city = ''
      matches31 = re.findall('.*&quot;city&quot;\:&quot;(.*?)&quot;,&quot.*', script_text)
      if matches31:          
        city = matches31[0]          

      matches3 = re.findall('.*&quot;,&quot;country&quot;\:&quot;(.*?)&quot;,&quot;display-address&quot;\:&quot;(.*?)&quot;,&quot;latitude.*', script_text)
      if matches3:                    
        country = matches3[0][0]
        address = matches3[0][1] 
      else:    
        matches3 = re.findall('.*&quot;country&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
        if matches3:                    
          country = matches3[0]          

      matches4 = re.findall('.*&quot;name&quot;\:&quot;(.*?)&quot;,&quot;privacy.*', script_text)
      if matches4:
        conference_name = matches4[0]

      conference_desc = ''
      matches5 = re.findall('.*&quot;description&quot;\:&quot;(.*?)&quot;,&quot;download-app-links.*', script_text)
      if matches5:
        conference_desc = matches5[0]

      matches6 = re.findall('.*&quot;agendaUrl&quot;\:&quot;(.*?)&quot;,&quot;associatedOnly.*', script_text)
      if matches6:
        agendaUrl = matches6[0]

      matches7 = re.findall('.*&quot;homepage-url&quot;\:&quot;(.*?)&quot;,&quot;.*', script_text)
      if matches7:
        conference_url = matches7[0]

      ""
      conf = []
      confs = self._air_confer_table.search('Name', str(conference_name))
      #print conf
      conf_fields = {
          'Notes': conference_desc,
          'Name': conference_name.replace('&amp;', '&'),
          'Year': conf_start_date.split('/')[-1],
          'Start Date': conf_start_date,
          'End Date': conf_end_date,
          'URL': conference_url,
          #'Agenda/People URL': agendaUrl,
          'City': city,
          'Country': country
        }

      #print conf_fields
      #self._exit()
      
      if not confs:        
        conf = self._air_confer_table.insert(conf_fields)
      else:
        conf = confs[0]
        self._air_confer_table.update(conf['id'], conf_fields)
      """
      conf_ID = meta['conf']['fields']['ID']
      conf_idx = meta['conf']['id']
      #self._exit()
      # Parse panels
      offset = 0
      meta = {
        'event_id': event_id, 
        'offset':offset,
        'conf_ID': conf_ID,
        'conf_idx': conf_idx
      }
      sessions_api = self.config['sessions_api'].format(event_id=event_id, offset=offset)
      req = scrapy.Request(sessions_api, callback=self.parse_sessions, meta = meta)
      yield req

    def parse_sessions(self, response):
      meta = response.meta
      if 'sessions_list' not in meta.keys():
        sessions_list = []
      else:
        sessions_list = meta['sessions_list']

      json_data = json.loads(response.body)

      bSpeaker = False
      if "sessions" in json_data.keys():
        # Parse sessions
        sessions = json_data['sessions']        
        for x in sessions:
          sessions_list.append(x)
        # Parse next page
        total = int(json_data['total'])
        count = int(json_data['count'])
        _offset = int(json_data['offset'])
        if (_offset + count) < total:
          offset = int(meta['offset'])
          offset = offset + 50        
          sessions_api = self.config['sessions_api'].format(event_id=meta['event_id'], offset=offset)
          meta.update({'offset':offset, 'sessions_list': sessions_list})
          req = scrapy.Request(sessions_api, callback=self.parse_sessions, meta = meta)
          yield req
        else:
          bSpeaker = True
      else:
        bSpeaker = True

      if bSpeaker:
        # Parse Speakers
        speakers_api = self.config['speakers_api'].format(event_id=meta['event_id'])
        meta.update({'sessions_list': sessions_list})
        req = scrapy.Request(speakers_api, callback=self.parse_speakers, meta=meta)
        yield req

    def parse_speakers(self, response):
      meta = response.meta
      json_data = json.loads(response.body)
      speakers_list = {}
      for x in json_data:
        speakers_list[x['id']] = x
      meta.update({'speakers_list': speakers_list})
      # Parse setting
      settings_api = self.config['settings_api'].format(event_id=meta['event_id'])
      req = scrapy.Request(settings_api, callback=self.parse_all, meta=meta)
      yield req

    def parse_all(self, response):
      meta = response.meta
      json_data = json.loads(response.body)
      locations_data = []
      locations = {}
      if 'locations' in json_data.keys():
        locations_data = json_data['locations']
      for x in locations_data:        
        locations[x['id']] = x

      filters = {}
      filters_data = json_data['filters']
      for x in filters_data:
        filters[x['id']] = x

      speakers_list = meta['speakers_list']
      sessions_list = meta['sessions_list']

      """ Insert into 'PANELS' """
      _air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)

      new_sessions_list = []
      for x in sessions_list:
        topic = string.capwords(x['title']).encode('unicode_escape')
        _id = str(x['id'])

        _search_term = str(topic) + str(' @ "') + str(meta['conf_ID']) + '"' + ' @ ' + 'pid' + _id
        search_term = _search_term.replace("'", r"\'")
        panels = _air_panels_table.search('Name', search_term, fields = ['Name'])
        
        start_date = str(datetime.datetime.utcfromtimestamp(int(x['startDate'])/1000).strftime('%m/%d/%Y'))
        end_date = str(datetime.datetime.utcfromtimestamp(int(x['endDate'])/1000).strftime('%m/%d/%Y'))
        start_time_string = start_date + ' ' + self.convert_hours(x['startMinute'])
        end_time_string = end_date + ' ' + self.convert_hours(x['endMinute'])

        _date = datetime.datetime.strptime(start_date, '%m/%d/%Y').strftime('%Y-%m-%d')

        panel_fields = {
          'Topic': topic,
          'Date': _date,
          'Link to Conference': [meta['conf_idx']],
          'Start Time': self.convert_time(start_time_string),
          'End Time': self.convert_time(end_time_string),
          'Panel ID': _id,            
          'URL of Panel': self.config['session_url'].format(event_id=meta['event_id'], session_id=_id)            
        }

        if 'description' in x.keys():
          panel_fields.update({'Description of Panel': x['description']})

        # Parse location
        final_loc = ''
        if 'locationId' in x.keys():
          loc_id = x['locationId']
          if loc_id in locations.keys():
            location = locations[loc_id]
            location_name = location['name']

            subloc_name = None
            if 'sublocationId' in x.keys():
              subloc_id = x['sublocationId']
              sublocations = location['sublocations']
              for subloc in sublocations:
                if subloc['id'] == subloc_id:
                  subloc_name = subloc['name']

            if subloc_name:
              final_loc = ', '.join([location_name, subloc_name])
            else:
              final_loc = location_name
        elif 'filters' in x.keys():
          _filters = x['filters']
          tags = []
          for _filter in _filters:            
            filter_id = _filter['id']            
            tags_id = _filter['tags']

            new_tags_list = {}
            tags_list = filters[filter_id]['tags']
            for t in tags_list:
              new_tags_list[t['id']] = t

            for tag_id in tags_id:
              _tag_name = new_tags_list[tag_id]['name']
              tag_name = re.sub(r'.*?(Track\s?\d+\s?\-?).*?', '', _tag_name)
              tags.append(tag_name.replace('Track', '').strip())
          if len(tags):
            final_loc = ', '.join(tags)
            print final_loc        

        #if final_loc != '':
        panel_fields.update({'Stage': final_loc})

        if not panels:                
          panel = _air_panels_table.insert(panel_fields)
        else:
          panel = panels[0]
          _air_panels_table.update(panel['id'], panel_fields)

        panel_id = panel['id']

        x.update({'panel_id': panel_id})

        new_sessions_list.append(x)    

      """ End Insert into 'PANELS' """
      
      """ Insert to table 'INDIVIDUALS' """
      self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)

      inserted_speakers_list = {}
      individual_role_at_panel_list = []      
      for x in new_sessions_list:
        if 'speakers' in x.keys():
          speakers = x['speakers']
          for spk in speakers:
            speakerId = spk['speakerId']
            if speakerId in speakers_list.keys():
              speaker = speakers_list[speakerId]
              role = spk['role'] if 'role' in spk.keys() else 'Panelist'

              """
              indiv_fields = {
                  'F. Name': speaker['firstname'],
                  'L. Name': speaker['lastname'],
                  'Pic': [{'url': speaker['thumbnailUrl'] if 'thumbnailUrl' in speaker.keys() else ''}],
                  'Linkedin': speaker['linkedinProfile'] if 'linkedinProfile' in speaker.keys() else '',
                  'Twitter': self.config['twitter_url'].format(twitterHandle=speaker['twitterHandle']) if 'twitterHandle' in speaker.keys() else '',
                  'Biography': speaker['bio'] if 'bio' in speaker.keys() else '',    
                  'Individual Link': self.config['speaker_url'].format(event_id=meta['event_id'], speaker_id=speakerId)
                }
              """

              firstname = speaker['firstname'].replace(u"\u2019", "'").encode('utf-8')
              lastname = speaker['lastname'].replace(u"\u2019", "'").encode('utf-8')
              full_name = ' '.join([firstname, lastname])
              search_speaker_name = str(full_name).replace("'", r"\'")

              indiv_fields = {
                  'F. Name': firstname,
                  'L. Name': lastname,
                  'Pic': [{'url': speaker['thumbnailUrl'] if 'thumbnailUrl' in speaker.keys() else ''}],
                  'Linkedin': speaker['linkedinProfile'] if 'linkedinProfile' in speaker.keys() else '',
                  'Twitter': self.config['twitter_url'].format(twitterHandle=speaker['twitterHandle']) if 'twitterHandle' in speaker.keys() else '',
                  'Biography': speaker['bio'] if 'bio' in speaker.keys() else '',    
                  'Individual Link': self.config['speaker_url'].format(event_id=meta['event_id'], speaker_id=speakerId)
                }


              indivs = self._air_indiv_table.search('Name', str(search_speaker_name))
              if not indivs:                            
                indiv = self._air_indiv_table.insert(indiv_fields)
                indiv_id = indiv['id']
              else:
                # Update person
                indiv_id = indivs[0]['id']              
                #update = self._air_indiv_table.update(indiv_id, indiv_fields)

              individual_role_at_panel_list.append({                  
                  'speaker_name': search_speaker_name,
                  'panel_id': x['panel_id'],
                  'indiv_id': indiv_id,
                  'role': str(role)
                })

              speaker.update({
                'indiv_id': indiv_id,
                'panel_id': panel_id,
                'role_at_panel': str(role),
                'search_speaker_name': search_speaker_name,
                'full_name': full_name
              })

              inserted_speakers_list[speakerId] = speaker

      """ End Insert to table 'INDIVIDUALS' """

      """ Insert person to 'INDIVIDUAL ROLE AT PANEL' """
      _air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)

      for x in individual_role_at_panel_list:
        panel_id = x['panel_id']
        indiv_id = x['indiv_id']
        role = x['role']
        
        role_at_panel_fields = {
          'Link to Panel': [panel_id],
          'Link to Person': [indiv_id],
          'Role': [role]
        }

        _person_name = str(x['speaker_name'])
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'
        indiv_role_at_panel_s = _air_role_panel_table.search('Link to Person', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_panel_s):          
          for i in indiv_role_at_panel_s:
            print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
            if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
              iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT PANEL'
          inserted = _air_role_panel_table.insert(role_at_panel_fields)

      """ End Insert person to 'INDIVIDUAL ROLE AT PANEL' """
      
      """ Insert 'COMPANIES' """      

      #_air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
      #self.all_companies = _air_company_table.get_all()
      
      inserted_companies = {}
      for x in self.all_companies:
        if 'Name' in x['fields'].keys():
          company_name = x['fields']['Name']

          clean_company_name = self.clean_company(company_name)
          company_id = x['id']   
          inserted_companies[clean_company_name] = company_id


      inserted_speakers_ids = inserted_speakers_list.keys()
      inserted_speakers = inserted_speakers_list.values()

      inserted_companies_keys = inserted_companies.keys()    

      for x in inserted_speakers:
        if 'company' not in x.keys():
          continue

        company_name = x['company']
        insert_com_fields = {
          'Name': company_name.strip()
        }
        clean_company_name = self.clean_company(company_name)

        if clean_company_name in inserted_companies_keys:
          company_id = inserted_companies[clean_company_name]
        else:
          # Insert company
          inserted_company = self._air_company_table.insert(insert_com_fields)
          self.all_companies.append(inserted_company)
          company_id = inserted_company['id']
          inserted_companies[clean_company_name] = company_id
          inserted_companies_keys.append(clean_company_name)

        x.update({'company_id': company_id})
        inserted_speakers_list[x['id']] = x

      """ End Insert 'COMPANIES' """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """

      _air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)
      for x in inserted_speakers_list.values():        
        indiv_id = x['indiv_id']
        if 'company_id' not in x.keys():
          continue

        company_id = x['company_id']

        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': ['Employee']
        }

        if 'title' in x.keys():
          if x['title'] != '':
            role_at_company_fields.update({'Official Title': x['title']})

        _person_name = str(x['search_speaker_name'])
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'

        indiv_role_at_company_s = _air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            if 'Link to Company' in i['fields'].keys():
              print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
              if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = _air_role_at_company_table.insert(role_at_company_fields)

      """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

      """ Parse individual details """

      """ End Parse individual details """


    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):   
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
