# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable
from scrapy.linkextractors import LinkExtractor

class DedupSponsorSpider(scrapy.Spider):
    name = "dedup_sponsor"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(DedupSponsorSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        #self.allowed_domains = ['*']

        #domain = get_domain(start_url)
        #self.allowed_domains.append(domain)
        #self.allowed_domains.append('ai-expo.net')
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)
        self._air_company_role_at_conf_table = Airtable(self.base_key, 'COMPANY ROLE AT CONF', api_key=self.api_key)

        # Get all companies
        #self.all_companies = self._air_company_table.get_all()
        self.all_companies = []
        self.all_sponsors = self._air_company_role_at_conf_table.get_all()

        self.iCnt = 0

        self.lx_sections = LinkExtractor(restrict_xpaths=('//div[@id="sched-container-header-menu"]//ul//li//a'),
                                allow=(r'/directory/sponsors'), deny=())

        self.lx_partners = LinkExtractor(restrict_xpaths=(),
                                allow=(r'/sponsor/'), deny=())

    def parse(self, response):            
      confs = self._air_confer_table.search('Backend', 'Bizzabo')
      _confs = self._air_confer_table.search('Backend', 'BlockchainExpo')
      confs.extend(_confs)
      _confs1 = self._air_confer_table.search('Backend', 'd10e')
      confs.extend(_confs1)
      _confs2 = self._air_confer_table.search('Backend', 'Sched')
      confs.extend(_confs2)
      
      conf_ids = []
      for x in confs:                
        conf_ids.append(x['id'])

      # Group confs by conf_id
      sponsor_groups = {}
      for sponsor in self.all_sponsors:
        if 'Link to Conference' not in sponsor['fields'].keys():
          continue
        conf_id = sponsor['fields']['Link to Conference'][0]
        if conf_id in conf_ids:
          if conf_id not in sponsor_groups.keys():
            sponsor_groups[conf_id] = [sponsor]
          else:
            sponsor_groups[conf_id].append(sponsor)

      # Group by company      
      for x in sponsor_groups.values():
        sponsor_company_groups = {}
        for y in x:
          company_id = y['fields']['Link to Company'][0]
          if company_id not in sponsor_company_groups.keys():
            sponsor_company_groups[company_id] = [y]
          else:
            sponsor_company_groups[company_id].append(y)

        for z in sponsor_company_groups.values():
          ok_groups = []
          del_groups = []
          for w in z:
            if 'Role' not in w['fields'].keys():
              continue
            if ('Sponsorship Level' in w['fields'].keys()) or ('Exhibitor' in w['fields']['Role']) \
                  or ('Organizer' in w['fields']['Role']) or ('Media Partner' in w['fields']['Role']) \
                  or ('Visitor' in w['fields']['Role']):
              ok_groups.append(w)
            else:
              del_groups.append(w)

          # Delete group
          if len(ok_groups) and len(del_groups):
            for s in del_groups:
              self.delete_sponsor(s['id'])

          if (not len(ok_groups)) and len(del_groups) > 1:
            for s in del_groups[1:]:
              self.delete_sponsor(s['id'])

          """
          if (len(ok_groups) == 1) and (len(del_groups) == 1):
            sponsor_id = ok_groups[0]['id']
            sponsor_role = ok_groups[0]['fields']['Role']

            delete_sponsor_role = del_groups[0]['fields']['Role']
          """

    def delete_sponsor(self, sponsor_id):
      self._air_company_role_at_conf_table.delete(sponsor_id)


    def get_sponsor_link(self, response):
      meta = response.meta
      for l in self.lx_sections.extract_links(response): 
        _url = l.url                  
        yield scrapy.Request(_url, self.get_partner_link, meta = meta) 

    def get_partner_link(self, response):
      meta = response.meta

      levels = response.xpath('.//div[contains(@class, "sched-container-sponsors-")]')

      for level in levels:
        classes = level.xpath('.//@class').extract_first()
        _class = classes.split(' ')[-1]
        level_no = _class.replace('sched-container-sponsors-', '')

        if level_no == "0":
          tier = "Tier 1"
        if level_no == "2":
          tier = "Tier 2"
        if level_no == "3":
          tier = "Tier 3"

        #print (level_no, tier)

        meta.update({'tier': tier})

        for _url in level.xpath('.//a[contains(@href, "/sponsor/")]//@href').extract():        
          #print _url         
          #domain = get_domain(_url)
          #if domain not in self.allowed_domains:
          #  self.allowed_domains.append(domain)        
          yield scrapy.Request(response.urljoin(_url), self.parse_partner, meta = meta)  

    def parse_partner(self, response):
      meta = response.meta

      company_names = response.xpath('.//h1[@id="sched-page-me-name"]//text()').extract()
      company_name = ' '.join([_x for _x in [x for x in company_names if x] if _x.replace('\n', '').replace('\r', '').strip()]).strip()
      if not company_name:
        return

      company_url = response.xpath('.//div[contains(@class, "sched-network-link-website")]//a//@href').extract_first()
      company_logo = response.urljoin(response.xpath('.//span[@id="sched-page-me-profile-avatar"]//img//@src').extract_first())
      company_logo = company_logo.split('?')[0]

      company_desc = ''
      company_descs = response.xpath('.//div[@id="sched-page-me-profile-about"]//text()').extract()
      if company_descs:
        company_desc = ''.join([x.replace('\n', '').replace('\r', '') for x in company_descs if x.replace('\n', '').replace('\r', '').strip()]).strip().encode('utf-8')    

      insert_com_fields = {
        'Name': str(company_name)              
      }

      if company_desc:
        insert_com_fields.update({
          'Company Description': company_desc
          })

      if company_url:
        insert_com_fields.update({'URL': str(company_url)})

      if company_logo:        
        insert_com_fields.update({
            'Logo': [{'url': company_logo}]
          })
      
      clean_company_name = self.clean_company(company_name)

      if clean_company_name in self.inserted_companies_keys:
        print "Updating"
        #print company_id, insert_com_fields
        company_id = self.inserted_companies[clean_company_name]
        # Update company info          
        self._air_company_table.update(company_id, insert_com_fields)
      else:
        # Insert company
        print "Inserting"
        inserted_company = self._air_company_table.insert(insert_com_fields)
        self.all_companies.append(inserted_company)
        company_id = inserted_company['id']
        self.inserted_companies[clean_company_name] = company_id
        self.inserted_companies_keys.append(clean_company_name)
        
      """ End Insert company """

      """ Insert company role at conf """      
      #company_role_at_conf = self._air_company_role_at_conf_table.search('Link to Conference', '"' + meta['conf']['fields']['ID'] + '"')
      company_role_at_conf = self._air_company_role_at_conf_table.search('Link to Conference', '"' + str(meta['conf']['fields']['ID']) + '"')
      
      inserted_role = {}

      for role in company_role_at_conf:
        inserted_role[role['fields']['Link to Company'][0]] = [role['id'], role['fields']['Role']]

      _role = ["Sponsor"]

      if company_id in inserted_role.keys():
        print "Updating sponsor........."
        _id = inserted_role[company_id][0]
        old_role = inserted_role[company_id][1]        
        for _r in old_role:
          _role.append(_r)
        # Update
        role_fields = {
          'Role': list(set(_role)),
          'Sponsorship Level': meta['tier']
          }
        self._air_company_role_at_conf_table.update(_id, role_fields)
      else:
        # Insert
        print "Inserting sponsor.........."
        role_fields = {
          'Link to Company': [company_id],
          'Link to Conference': [meta['conf']['id']],
          'Role': _role,
          'Sponsorship Level': meta['tier']
          }
        self._air_company_role_at_conf_table.insert(role_fields)

      """ End Insert company role at conf """

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M%p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
