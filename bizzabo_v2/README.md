Required:
- Python 2.7
- Scrapy installed
- Selenium installed
- Chrome webdriver

*** Install required libs ***
pip install -r requirements.txt

==========================================

- Extract this package
- Config params in config.json file


- Open command window(Windows) or Terminal(Linux) then move to this folder
- Run command: 
For windows:  

+ Organize Conferences by backend
  
  scrapy crawl backend

+ Scrape conference, panels, individuals, individual role at panel, companies, individual role at company
  
  scrapy crawl bizzabo

+ Scrape individual email:
  
  scrapy crawl speaker

+ Mapping video to panel

  scrapy crawl video

+ Scrape sponsors, company role at conference

  scrapy crawl sponsor


- That's all




