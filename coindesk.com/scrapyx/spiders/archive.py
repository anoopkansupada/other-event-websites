# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class ArchiveSpider(scrapy.Spider):
    name = "archive"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    list_url = [
      'https://web.archive.org/web/20140601000000*/https://www.coindesk.com/bitcoin-events/',
      'https://web.archive.org/web/20150101000000*/https://www.coindesk.com/bitcoin-events/',
      'https://web.archive.org/web/20161101000000*/https://www.coindesk.com/bitcoin-events/',
      'https://web.archive.org/web/20170701000000*/https://www.coindesk.com/bitcoin-events/',
      'https://web.archive.org/web/20180501000000*/https://www.coindesk.com/bitcoin-events/'
    ]

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(ArchiveSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        #self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        #self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        #self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        #self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        #self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        #self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all conferences
        #self.all_confs = self._air_confer_table.get_all()

        self.iCnt = 0

    def parse(self, response):    
      list_archive_url = []
      for url in self.list_url:
        self.driver.get(url)
        wait_for_element(self.driver, '#wb-calendar')

        responsex = TextResponse(url=self.driver.current_url, body=self.driver.page_source, encoding='utf-8')
        for a in responsex.xpath('.//div[@class="captures"]//a[contains(@href, "coindesk.com/bitcoin-events")]'):
          _url = a.xpath('.//@href').extract_first()
          list_archive_url.append(responsex.urljoin(_url))

      for x in list_archive_url:
        self.saveUrl('list_url.txt', x)

    def saveUrl(self, file, url):        
      bSaved = False
      url = url.strip()
      f = open(file, 'r+')
      lines = f.readlines()
      f.seek(0)
      f.truncate()
      
      for line in lines: 
        line = line.strip()
        f.write(line + '\n')
        if url == line:
          bSaved = True                
                              
      if bSaved == False:
        f.write(url + '\n')
        
      f.close()
      
      return bSaved

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class CoindeskSpider(scrapy.Spider):
    name = "coindesk"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(CoindeskSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        #self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        #self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        #self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        #self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        #self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all conferences
        self.all_confs = self._air_confer_table.get_all()

        self.iCnt = 0

    def parse(self, response):    
      index = 0 
      for tr in response.xpath('.//table[@class="table"]//tbody//tr'):  
        index += 1
        times = tr.xpath('.//td[1]//text()').extract_first().strip()
        time_part = times.split(',')
        date = ''
        if len(time_part) > 1:
          year = time_part[-1].strip()
          date = time_part[0].strip()
        else:
          time_part = times.split(' ')
          if len(time_part) > 1:
            year = time_part[-1].strip()
            if len(year) == 4:
              date = ' '.join(time_part[:-1])

        start_date = ''
        end_date = ''
        if date:
          date_part = date.split('-')
          date1 = date_part[0].strip()
          date1_part = date1.split(' ')          
          if len(date1_part) > 1:
            start_date = ' '.join([date1_part[0], date1_part[-1], year])
            date2 = date_part[-1].strip()
            date2_part = date2.split(' ')
            if len(date2_part) > 1:
              end_date = ' '.join([date2_part[0], date2_part[-1], year])
            else:
              end_date = ' '.join([date1_part[0], date2, year])

        if start_date:
          start_date = datetime.datetime.strptime(start_date, '%B %d %Y').strftime('%Y-%m-%d')
        if end_date:
          end_date = datetime.datetime.strptime(end_date, '%B %d %Y').strftime('%Y-%m-%d')

        name = tr.xpath('.//td[2]//a[1]//text()').extract_first().encode('unicode_escape')
        conf_url = tr.xpath('.//td[2]//a[1]//@href').extract_first()
        location = tr.xpath('.//td[3]//text()').extract_first()
        location_part = location.split(',')
        city = ''
        country = ''
        if len(location_part) > 1:
          city = location_part[0].strip().encode('unicode_escape')
          country = location_part[-1].strip().encode('unicode_escape')
        else:
          country = location.strip().encode('unicode_escape')
          city = location.strip().encode('unicode_escape')
      
        print (start_date, end_date, name, conf_url, city, country)

        search_term = name + ' (' + ', '.join([city, year]) + ')'
        
        bExist = False
        conf = None
        for x in self.all_confs:
          ID = x['fields']['ID']
          if ID.lower() == search_term.lower():
            bExist = True
            conf = x
            break

        #search_term = search_term.replace("'", r"\'")
        #confs = self._air_confer_table.search('ID', str(search_term))

        conf_fields = {          
          'Name': name.replace('&amp;', '&'),
          'Year': year,
          'URL': conf_url,          
          'City': city,
          'Country': country,
          'Notes': 'HB added 8/1'
        }

        if start_date:
          conf_fields.update({'Start Date': start_date})
        if end_date:
          conf_fields.update({'End Date': end_date})

        if not bExist:        
          conf = self._air_confer_table.insert(conf_fields)
        else:          
          self._air_confer_table.update(conf['id'], conf_fields)
      print index

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      clean_company_name = ' '.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
