# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ArticleItem(scrapy.Item):
    url = scrapy.Field()
    name_of_the_card = scrapy.Field()
    price = scrapy.Field()    
    number_left_in_stock = scrapy.Field()  
    keyword = scrapy.Field()
    