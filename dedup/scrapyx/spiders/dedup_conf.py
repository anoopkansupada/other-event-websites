# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 
import hashlib

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class DedupConfSpider(scrapy.Spider):
    name = "dedup_conf"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(DedupConfSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        #self.all_companies = self._air_company_table.get_all()
        self.all_companies = []

        self.iCnt = 0

        self.confs = []
        self.confs_groups = {}
        _confs = self._air_confer_table.get_all()
        print len(_confs)
        for x in _confs:
          if 'URL' in x['fields'].keys():
            self.confs.append(x)

    def parse(self, response):        
      # Group by URL, start_date, end_date, city           
      for x in self.confs:
        url = x['fields']['URL'].strip()        
        start_date = ''
        if 'Start Date' in x['fields'].keys():
          start_date = x['fields']['Start Date'].strip()

        end_date = ''
        if 'End Date' in x['fields'].keys():
          end_date = x['fields']['End Date'].strip()

        city = ''
        if 'City' in x['fields'].keys():
          city = x['fields']['City'].strip()

        #_string = ''.join([url, start_date, end_date, city])
        _string = ''.join([url, start_date, end_date])
        md5_string = self.md5hash(_string)

        if md5_string not in self.confs_groups.keys():
          self.confs_groups[md5_string] = [x]
        else:
          self.confs_groups[md5_string].append(x)

      # process
      for group in self.confs_groups.values():
        ok_groups = []
        del_groups = []
        for conf in group:
          if ('PANELS' in conf['fields'].keys()) or ('INDIVIDUALS' in conf['fields'].keys()) or ('Role @ Event' in conf['fields'].keys()):
            ok_groups.append(conf)
          else:
            del_groups.append(conf)    

        # Delete group
        if len(ok_groups) and len(del_groups):
          for x in del_groups:
            self.delete_conf(x['id'])

        if (not len(ok_groups)) and len(del_groups):
          for x in del_groups[1:]:
            self.delete_conf(x['id'])


    def delete_conf(self, conf_id):
      self._air_confer_table.delete(conf_id)

    def md5hash(self, _string):        
      _hex = hashlib.md5(_string.encode('utf-8')).hexdigest()
      return _hex

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_3(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string.upper(), '%Y-%m-%d %I:%M%p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
