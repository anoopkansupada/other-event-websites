# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable

class AgendaSpider(scrapy.Spider):
    name = "agenda"
    allowed_domains = []
    start_urls = [
      'https://www.blockchainseattle.io/blockchain-seattle-2018-conference-schedule/',
      'https://www.blockchainseattle.io/blockchain-seattle-2018-conference-schedule-2/'
    ]    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(AgendaSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        #domain = get_domain(start_url)
        #self.allowed_domains.append(domain)
        #self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        self.all_companies = []
        #self.all_companies = self._air_company_table.get_all()

        self.iCnt = 0
        self.conf_id= 'recpwZIkrXZTGoGgg'

    def parse(self, response):  

      
      confs = self._air_confer_table.search('Backend', 'Keynote')
      
      for x in confs:                
        _url = x['fields']['URL']   
        meta = {
          'conf_id': 'recpwZIkrXZTGoGgg',
        }
        """
        if 'newyork.keynote.ae' in _url:
          if '/agenda/' not in _url:
            _url = _url + 'agenda/'
          req = scrapy.Request(_url, callback=self.parse_agenda, meta = meta)
          yield req

        """
        if '/agenda/' not in _url:
          _url = _url + 'agenda/'
        req = scrapy.Request(_url, callback=self.parse_agenda, meta = meta)
        yield req

        #"""

    def parse_agenda(self, response):
      meta = response.meta
      if response.xpath('.//div[contains(@class, "gdlr-tab-1")]'):
        # Parse sessions page
        for div in response.xpath('.//div[contains(@class, "gdlr-session-item-tab-content")]//div[@class="gdlr-session-item-content-wrapper"]'):
          session_link = div.xpath('.//h3[@class="gdlr-session-item-title"]//a//@href').extract_first()
          req = scrapy.Request(response.urljoin(session_link), callback=self.parse_session, meta = meta)
          yield req
      else:
        # Parse speakers page only
        original_url = meta['original_url']
        if '/speakers/' not in original_url:
          _url = original_url + 'speakers/'
          req = scrapy.Request(_url, callback=self.parse_speakers_page, meta = meta, dont_filter=True)
          yield req

    def parse_speakers_page(self, response):
      meta = response.meta

      for a in response.xpath('.//a[@class="gdlr-speaker-thumbnail-overlay-link"]'):
        speaker_link = a.xpath('.//@href').extract_first()
        req = scrapy.Request(response.urljoin(speaker_link), callback=self.parse_individual, meta = meta, dont_filter=True)
        yield req      

    def parse_session(self, response):
      meta = response.meta      
      session_id = response.xpath('.//div[contains(@id, "session-")]//@id').extract_first().split('-')[-1]

      topic = response.xpath('.//h4[@class="gdlr-session-title"]//text()').extract_first().encode('unicode_escape')
      desc = response.xpath('.//div[@class="gdlr-session-content"]/p[1]//text()').extract_first()

      session_times = response.xpath('.//div[@class="gdlr-session-info"]//div[contains(@class, "session-time")]')
      date = session_times[0].xpath('.//text()').extract()[-1]
      times = session_times[1].xpath('.//text()').extract()[-1]
      times_part = times.split('-')

      location = ''
      locations = response.xpath('.//div[@class="gdlr-session-info"]//div[contains(@class, "session-location")]//text()').extract()
      if locations:
        location = locations[-1]

      speakers = response.xpath('.//div[@class="gdlr-session-info"]//div[@class="session-speaker-list"]//div[@class="session-speaker-list-item"]//a//@href').extract()

      # Insert into Panels
      topic = string.capwords(topic)
      _search_term = str(topic) + str(' @ "') + str(meta['conf']['fields']['ID']) + '"' + ' @ ' + 'pid' + str(session_id)
      search_term = _search_term.replace("'", r"\'")
      panels = self._air_panels_table.search('Name', search_term, fields = ['Name'])
      
      _date = datetime.datetime.strptime(string.capwords(date), '%d %b %Y').strftime('%Y-%m-%d')
      _date_only = datetime.datetime.strptime(_date, '%Y-%m-%d').strftime('%m/%d/%Y')
      #print (_date, times_part)
      
      start_time_string = str(_date) + ' ' + times_part[0].strip()

      end_time_string = ''
      if len(times_part) > 1:
        end_time_string = str(_date) + ' ' + times_part[1].strip()  

      panel_fields = {
        'Topic': topic,
        'Date': str(_date_only),
        'Link to Conference': [meta['conf']['id']],
        'Start Time': start_time_string,        
        'Panel ID': session_id,            
        'URL of Panel': response.url,
        'Description of Panel': desc if desc else '',
        'Stage': location
      }

      if end_time_string:
        panel_fields.update({
            'End Time': end_time_string,
          })

      if not panels:                
        panel = self._air_panels_table.insert(panel_fields)
      else:
        panel = panels[0]
        self._air_panels_table.update(panel['id'], panel_fields)

      panel_id = panel['id']
      meta.update({
          'panel_id': panel_id
        })

      #print (session_id, topic, desc, date, times, speakers, panel_id)
      for x in speakers:
        req = scrapy.Request(response.urljoin(x), callback=self.parse_individual, meta = meta, dont_filter=True)
        yield req


    def parse_individual(self, response):
      meta = response.meta

      speaker_name = response.xpath('.//h4[@class="gdlr-speaker-name"]//text()').extract_first()
      if speaker_name:
        speaker_name = string.capwords(speaker_name).encode('utf-8')
      else:
        return

      speaker_position = response.xpath('.//div[@class="gdlr-speaker-position"]//text()').extract_first()
      company = ''
      speaker_role = ''
      if speaker_position:
        speaker_position_parts = speaker_position.split(' at ')
        if len(speaker_position_parts) < 2:
          speaker_position_parts = speaker_position.split(',')
        
        if len(speaker_position_parts) > 1:
          speaker_role = speaker_position_parts[0].strip()
          company = speaker_position_parts[1].strip()
        else:
          speaker_role = speaker_position

      bio = ''
      bios= response.xpath('.//div[@class="gdlr-speaker-content"]//text()').extract()
      if bios:        
        aBio = [x.strip() for x in bios if x]
        newBio = []
        for t in aBio:
          if t and t[-1] == '.':
            t = t + '\n'

          if t:
            newBio.append(t)

        bio = ' '.join(newBio).encode('utf-8')     

      speaker_photo = response.xpath('.//div[@class="gdlr-speaker-thumbnail"]//a//@href').extract_first()
      
      name_parts = speaker_name.split(' ')
      firstname = ' '.join(name_parts[:-1])
      lastname = name_parts[-1]

      """ Insert to table 'INDIVIDUALS' """
      indiv_fields = {
        'F. Name': firstname,
        'L. Name': lastname,                
        'Biography': bio if bio else '',    
        'Individual Link': response.url,
        'Link to Conf': [meta['conf']['id']]
      }

      if speaker_photo:
        indiv_fields.update({
            'Pic': [{'url': speaker_photo}],
          })

      search_speaker_name = speaker_name.replace(u"\u2019", "'").encode('utf-8').replace("'", r"\'")

      indivs = self._air_indiv_table.search('Name', str(search_speaker_name))

      if not indivs:                            
        indiv = self._air_indiv_table.insert(indiv_fields)
        indiv_id = indiv['id']
      else:
        # Update person
        indiv_id = indivs[0]['id']  
        update = self._air_indiv_table.update(indiv_id, indiv_fields)
      """ End Insert to table 'INDIVIDUALS' """

      """ Insert person to 'INDIVIDUAL ROLE AT PANEL' """
      if 'panel_id' in meta.keys():
        panel_id = meta['panel_id']
        role_at_panel_fields = {
              'Link to Panel': [panel_id],
              'Link to Person': [indiv_id],
              'Role': ['Speaker']
            }

        if ',' in search_speaker_name:
          _person_name = '"' + search_speaker_name + '"'
        else:
          _person_name = search_speaker_name
        indiv_role_at_panel_s = self._air_role_panel_table.search('Link to Person', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_panel_s):          
          for i in indiv_role_at_panel_s:
            print str(i[u'fields'][u'Link to Panel'][0]), ' ---- ', str(panel_id)
            if str(i[u'fields'][u'Link to Panel'][0]) == str(panel_id):
              self._air_role_panel_table.update(i['id'], role_at_panel_fields)
              iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT PANEL'
          inserted = self._air_role_panel_table.insert(role_at_panel_fields)

      """ End Insert person to 'INDIVIDUAL ROLE AT PANEL' """

      """ Insert 'COMPANIES' """  

      if company != '':    
        inserted_companies = {}
        for x in self.all_companies:
          if 'Name' in x['fields'].keys():
            company_name = x['fields']['Name']

            clean_company_name = self.clean_company(company_name)
            company_id = x['id']   
            inserted_companies[clean_company_name] = company_id        

        inserted_companies_keys = inserted_companies.keys()    
        
        insert_com_fields = {
          'Name': company.strip()
        }
        clean_company_name = self.clean_company(company)

        if clean_company_name in inserted_companies_keys:
          company_id = inserted_companies[clean_company_name]
        else:
          # Insert company
          inserted_company = self._air_company_table.insert(insert_com_fields)
          company_id = inserted_company['id'] 
          self.all_companies.append(inserted_company)           

      """ End Insert Company """

      """ Insert 'INDIVIDUAL ROLE AT COMPANY' """
      if company and speaker_role:
        role_at_company_fields = {
          'Link to Individual': [indiv_id],
          'Link to Company': [company_id],
          'Role': ['Employee'],
          'Official Title': speaker_role
        }

        _person_name = str(search_speaker_name)
        if ',' in _person_name:
          _person_name = '"' + _person_name + '"'
        else:
          _person_name = search_speaker_name

        indiv_role_at_company_s = self._air_role_at_company_table.search('Link to Individual', _person_name)
        
        iCnt = 0
        if len(indiv_role_at_company_s):          
          for i in indiv_role_at_company_s:
            if 'Link to Company' in i['fields'].keys():
              print str(i[u'fields'][u'Link to Company'][0]), ' ---- ', str(company_id)
              if str(i[u'fields'][u'Link to Company'][0]) == str(company_id):
                iCnt += 1                      

        if iCnt == 0:
          # Insert to 'INDIVIDUAL ROLE AT COMPANY'
          inserted = self._air_role_at_company_table.insert(role_at_company_fields)

        """ End Insert 'INDIVIDUAL ROLE AT COMPANY' """

    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M %p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
