# -*- coding: utf-8 -*-
import MySQLdb
import json
from scrapy.utils.project import get_project_settings


settings = get_project_settings()
with open('config.json', 'r') as ip:
    _config = json.loads(ip.read())            
    config = _config[_config['environment']]    
    db_config = config['db']

def init_connection():   
    conn = MySQLdb.connect(user=db_config.get('DB_USER'), passwd=db_config.get('DB_PASS'), db=db_config.get('DB_NAME'), host=db_config.get('DB_HOST'), charset="utf8", use_unicode=True)
    cursor = conn.cursor()
    return conn
    
def insert(item, conn, db_table):
    cursor = conn.cursor()
    aFormat = []                
    for key in item.keys():
        aFormat.append("%s")            
        
    sFormat = ', '.join(aFormat)
    
    fields = ', '.join(item.keys())
    insert_id = 0
    try:
        cursor.execute("""INSERT INTO """ + db_table + """(""" + fields + """)  
                        VALUES (""" + sFormat + """)""", 
                       item.values())
        insert_id = conn.insert_id()
        conn.commit()
    except MySQLdb.Error, e:
        print ("Error %d: %s" % (e.args[0], e.args[1]))
        conn.rollback()
      
    return insert_id

def insert_multiple(connection, table_name, data):
    cursor = connection.cursor()
    aFormat = []        

    sColumns = ', '.join(data[0].keys())

    for key in data[0].keys():
      aFormat.append("%s")            
    sFormat = ', '.join(aFormat)    

    values_to_insert = [x.values() for x in data]
    
    _return = False
    try:           
        _query = "INSERT INTO " + table_name + "(" + sColumns + ") VALUES " + ",".join("(" + sFormat + ")" for _ in values_to_insert)
        flattened_values = [item for sublist in values_to_insert for item in sublist]
        cursor.execute(_query, flattened_values)

        #args_str = ','.join(cursor.mogrify("(" + sFormat + ")", x.values()) for x in data)
        #result = cursor.execute("INSERT INTO " + table_name + "(" + sColumns + ") VALUES " + args_str)       
   
    except MySQLdb.Error, e:
        print ("Error %d: %s" % (e.args[0], e.args[1]))
        connection.rollback()

    finally:
      connection.commit()
      _return = True

    return _return
    
def query(conn, query):
    cursor = conn.cursor()
    try:
        result = cursor.execute(query)        
        conn.commit()
        return result
    except MySQLdb.Error, e:
        print ("Error %d: %s" % (e.args[0], e.args[1]))
        conn.rollback()
        
def select(conn, query):
    cursor = conn.cursor()      
    cursor.execute(query)
    rows = cursor.fetchall()
    return rows
    
        