# -*- coding: utf-8 -*-
#!/bin/sh
import sys
import re
import string
import scrapy
import csv
import logging
import json
import urllib
from scrapyx.utils import * 
from scrapy.selector import Selector
from scrapyx.items import ArticleItem
from scrapy.http import TextResponse 
from scrapy import Request, FormRequest
import scrapy
import time
import datetime
import pytz
import dateutil.parser as dateparser

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException    
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

from PIL import Image
from time import sleep
from datetime import date
from random import uniform, randint

import ast
#from scrapyx.postgres import Postgres
from scrapyx.mysql import * 

try:
    from pyvirtualdisplay import Display
except:
    print ("Cannot import Display")

from airtable import Airtable
from scrapy.linkextractors import LinkExtractor

class TokendataSpider(scrapy.Spider):
    name = "tokendata"
    allowed_domains = []
    start_urls = []    
    config = []        
    driver = None    
    base_key = None
    api_key = None   
    event_id = None
    sessions_list = []
    speakers_list = {}
    inserted_speakers_list = {}
    locations = {}
    filters = {}

    conf_ID = None
    conf_idx = None
    _air_indiv_table = None
    _air_confer_table = None
    logo_format = "https://www.tokendata.io/assets/logos/{company_name}.png"

    def __init__(self, proxy_opt = None, url = None, *args, **kwargs):
        super(TokendataSpider, self).__init__(*args, **kwargs)
        with open('config.json', 'r') as ip:
            _config = json.loads(ip.read())            
            self.config = _config[_config['environment']]                          

        if url != None:
          start_url = url
        else:
          start_url = self.config['url']

        domain = get_domain(start_url)
        self.allowed_domains.append(domain)
        self.start_urls.append(start_url)        

        self.base_key = self.config['airtable_base_key']
        self.api_key = self.config['airtable_api_key']

        if proxy_opt != None:
          self.proxyOpt = int(proxy_opt)   
          self.driver = init_web_driver(self.proxyOpt)
        
        try:
            display = Display(visible=0, size=(800, 600))
            display.start()
        except:
            pass                

        self._air_confer_table = Airtable(self.base_key, 'CONFERENCE', api_key=self.api_key)
        self._air_panels_table = Airtable(self.base_key, 'PANELS', api_key=self.api_key)
        self._air_indiv_table = Airtable(self.base_key, 'INDIVIDUALS', api_key=self.api_key)
        self._air_role_panel_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT PANEL', api_key=self.api_key)
        self._air_company_table = Airtable(self.base_key, 'COMPANIES', api_key=self.api_key)
        self._air_role_at_company_table = Airtable(self.base_key, 'INDIVIDUAL ROLE AT COMPANY', api_key=self.api_key)

        # Get all companies
        self.all_companies = self._air_company_table.get_all()

        self.iCnt = 0

        self.lx_sections = LinkExtractor(restrict_xpaths=('//div[@class="navbar-nav"]//ul//li//a'),
                                allow=r'/track/', deny=())

    def parse(self, response):   
      inserted_companies = {}
      for x in self.all_companies:
        if 'Name' in x['fields'].keys():
          company_name = x['fields']['Name']

          clean_company_name = self.clean_company(company_name)
          company_id = x['id']   
          inserted_companies[clean_company_name] = company_id        

      inserted_companies_keys = inserted_companies.keys()

      json_data = json.loads(response.body)   
      data = json_data['data']
      
      for x in data:
        keys = x.keys()
        company = x['name']
        month = x['month']
        symbol = x['symbol'] if 'symbol' in keys else ''
        whitepaper = x['whitepaper']
        status = x['status']
        website = x['website']
        description = x['description']
        usd_raised = x['usd_raised']
        token_sale_price = x['token_sale_price']

        """ Insert 'COMPANIES' """  

        if company:                                  
          if status == 'Completed':
            ico_completed = "Yes"
          else:
            ico_completed = "No"

          insert_com_fields = {
            'Name': company.strip(),
            'Month': month,
            'Symbol': symbol,
            'USD Raised': str(usd_raised),
            'ICO Price': str(token_sale_price),
            'ICO Completed': ico_completed
          }

          if website:
            insert_com_fields.update({'URL': website})

          if description:
            insert_com_fields.update({'Company Description': description})

          company_logo = self.logo_format.format(company_name=company.lower())
          if company_logo:
            insert_com_fields.update({
                'Logo': [{'url': company_logo}]
              })

          if whitepaper:
            insert_com_fields.update({
                'Whitepaper': [{'url': whitepaper}]
              })

          #print insert_com_fields

          clean_company_name = self.clean_company(company)

          if clean_company_name in inserted_companies_keys:
            # Update company
            company_id = inserted_companies[clean_company_name]
            print 'Updating'
            self._air_company_table.update(company_id, insert_com_fields)
          else:
            # Insert company
            print 'Inserting'
            inserted_company = self._air_company_table.insert(insert_com_fields)
            company_id = inserted_company['id'] 
            inserted_companies_keys.append(clean_company_name)    
            inserted_companies[clean_company_name] = company_id    

        """ End Insert Company """

    
    def clean_company(self, company_name):
      business_entities = ['llp', 'llp.', 'lp', 'lp.', 'llc', 'llc.', \
        'ltd', 'ltd.', 'co.', 'inc', 'inc.', 'pte', 'pte.',\
        'ag', 'ag.', 'corp', 'corp.', 'pvt', 'pvt.']

      company_name = company_name.replace(u"\u2019", "'").encode('utf-8').lower()
      company_name = company_name.replace(',', ' ')
      company_parts = [x for x in company_name.split(' ') if x.strip()]
      company_parts_clean = list(set(company_parts) - set(business_entities))

      #clean_company_name = ' '.join(company_parts_clean)
      clean_company_name = ''.join(company_parts_clean)
      return clean_company_name

    def convert_hours(self, minutes):
      hour = minutes/60
      minute = minutes%60
      _return = str(hour) + ':' + str(minute)
      return _return
      
    def convert_time(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%Y-%m-%d %H:%M')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return

    def convert_time_2(self, time_string, timezone = 'America/New_York'):      
      datetime_object = datetime.datetime.strptime(time_string, '%m/%d/%Y %I:%M%p')
      _return = str(datetime_object.isoformat()) + ".000Z"
      #print _return
      return _return
    
    def wait_between(self, a, b):
      rand=uniform(a, b) 
      sleep(rand)

    def closed(self, reason = None):  
      print self.iCnt 
      if reason:
        print reason
      
      sendAlert()
      if self.driver:
        self.driver.close()

    def _exit(self):
      self.closed()
      sys.exit()
